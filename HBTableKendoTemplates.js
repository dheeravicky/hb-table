function GussetTemplate(data) {
    return "<div class='row onerowheight'>" +
        "<div class='column onetd'>" +
        "<span class='beamSpan'><b>Thickness</b><b class='beamMargin'>:</b>".concat(data.Gusset.Thickness) + "</span>" +
        "<span class='beamSpan'><b>Grade</b><b class='beamMargin'>:</b>".concat(data.Gusset.Grade) + "</span>" +
        "</div>"

}

function BraceProfileTemplate(data) {

    if (data.BraceProfile.rows) {

        var rowClass = "rowstyle";
        var divCode = "";
        data.BraceProfile.rows.forEach(function (braceObj) {

            divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";
            divCode += "<span class='beamSpan'>".concat(braceObj) + "</span>";
            divCode += "</div></div>";

            rowClass = "secondrow";

        });
        return divCode;
    } else {
        return data.BraceProfile;
    }

}


function RowTemplate(dataArray) {

    if (dataArray.rows) {

        var rowClass = "rowstyle";
        var divCode = "";
        dataArray.rows.forEach(function (braceDetailObj) {

            divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";
            braceDetailObj.forEach(function (obj) {
                divCode += "<span class='beamSpan'><b>" + obj.key + "</b><b class='beamMargin'>:</b>".concat(obj.value) + "</span>";
            });
            divCode += "</div></div>";

            rowClass = "secondrow";

        });
        return divCode;
    }
    else {
        var divCode = "<div class='row onerowheight'>" +
            "<div class='column onetd'>";

        dataArray.forEach(function (obj) {
            divCode += "<span class='beamSpan'><b>" + obj.key + "</b><b class='beamMargin'>:</b>".concat(obj.value) + "</span>";
        });

        return divCode += "</div></div>";
    }
}


function B2GBraceConnectionTemplate(data) {

    if (data.B2GBraceConnection.rows) {

        var rowClass = "rowstyle";
        var divCode = "";
        data.B2GBraceConnection.rows.forEach(function (braceConnObj) {
            divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";

            divCode += B2GBraceConnectionTemplateBuilder(braceConnObj);

            divCode += "</div></div>";

            rowClass = "secondrow";

        });

        return divCode;

    }
    else {
        return B2GBraceConnectionTemplateBuilder(data.B2GBraceConnection);
    }

}
function B2GBraceConnectionTemplateBuilder(B2GBraceConnection) {


    if (typeof (B2GBraceConnection) == 'object') {
        var divCode = "";
        var rowClass = "rowstyle";
        B2GBraceConnection.forEach(function (B2Gobj) {

            divCode += "<div class='" + rowClass + " tworowheight'>" +
                "<div class='column onetd'>";
            B2Gobj.rows.forEach(function (obj) {
                divCode += "<span class='beamSpan'><b>" + obj.key + "</b><b class='beamMargin'>:</b>".concat(obj.value) + "</span>";
            });

            divCode += "</div></div>";
            rowClass = "secondrow";
        });
        return divCode;

    }
    else {
        return B2GBraceConnection;
    }

}

function getColumnTdCountCss(arrayObj) {

    if (arrayObj.length == 1) {
        return "onetd";
    }
    else if (arrayObj.length == 2) {
        return "twotd";
    }
    else if (arrayObj.length == 3) {
        return "threetd";
    }
}
function BoltedTemplate(BoldetObj) {

    if (BoldetObj.rows) {
        var rowClass = "rowstyle";
        var divCode = "";
        BoldetObj.rows.forEach(function (B2GBoltedObj) {
            divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";

            divCode += BoltedTemplateBuilder(B2GBoltedObj);
            divCode += "</div></div>";
            rowClass = "secondrow";

        });
        return divCode;

    }
    else {
        return BoltedTemplateBuilder(BoldetObj);
    }
}
function BoltedTemplateBuilder(B2GBolted) {

    var divCode = "";
    var colClass = getColumnTdCountCss(B2GBolted);

    divCode += "<div class='row tworowheight'>" ;
    B2GBolted.forEach(function (boltObj) {

        divCode +=  "<div class='column " + colClass + "'>";

        if (boltObj.cols.rows) {

            var rowClass = "rowstyle";
            boltObj.cols.rows.forEach(function (rowObj) {

                divCode += "<div class='" + rowClass + " tworowheight'>" +
                    "<div class='column onetd'>";
                rowObj.data.forEach(function (obj) {
                    divCode += "<span class='beamSpan'><b>" + obj.key + "</b><b class='beamMargin'>:</b>".concat(obj.value) + "</span>";
                });

                divCode += "</div></div>";
                rowClass = "secondrow";

            });

        } else {

            boltObj.cols.forEach(function (obj) {
                divCode += "<span class='beamSpan'><b>" + obj.key + "</b><b class='beamMargin'>:</b>".concat(obj.value) + "</span>";
            });

        }
        divCode += "</div>";
    });
    divCode += "</div>";
    return divCode;
}

function G2BPriorityTemplate(G2BPriority) {

    var rowClass = "rowstyle";
    var divCode = "";
    G2BPriority.forEach(function (obj) {

        divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";
        if (obj) {
            divCode += "<span class='beamSpan'><b>" + obj.key + "</b><b class='beamMargin'>:</b>".concat(obj.value) + "</span>";

        }        
        divCode += "</div></div>";
        rowClass = "secondrow";

    });
    return divCode;

}

function G2BConnectionMethodTemplate(G2BConnectionMethod) {

    var rowClass = "rowstyle";
    var divCode = "";
    G2BConnectionMethod.rows.forEach(function (obj) {
        divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";
        if (obj.key) {
            divCode += "<span class='beamSpan'><b>" + obj.key + "</b><b class='beamMargin'>:</b>".concat(obj.value) + "</span>";
        }
        divCode += "</div></div>";
        rowClass = "secondrow";

    });
    return divCode;

}

function G2BBoltedTemplate(G2BBolted) {

    var rowClass = "rowstyle";
    var divCode = "";
    G2BBolted.rows.forEach(function (obj) {
        divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";

        if (obj) {
            divCode += BoltedTemplateBuilder(obj);
        }

        divCode += "</div></div>";
        rowClass = "secondrow";

    });
    return divCode;
}

function RowTemplateG2B(item) {

    var rowClass = "rowstyle";
    var divCode = "";
    item.rows.forEach(function (obj) {
        divCode += "<div class='" + rowClass + " tworowheight'><div class='column onetd'>";

        if (obj) {
            divCode += RowTemplate(obj);
        }

        divCode += "</div></div>";
        rowClass = "secondrow";

    });
    return divCode;

}