function BuildHbKendoGridJson(sourceJson) {
    var data = [];
    if (sourceJson) {

        $.each(sourceJson, function (index, item) {
            var row = {
                SNo: index + 1,
                ConnectionMark: item.ConnectionMark,
                ConnectionType: item.ConnectionType,
                BraceProfile: getBraceProfile(item),
                Gusset: getGussetDetails(item),
                B2GDetails: getB2GDetails(item),
                B2GBraceConnection: getB2GBraceConnectionMetod(item),
                B2GBolted: getB2GBBolted(item),
                B2GEDdgeDistance: getB2GEdgeDistance(item),
                B2GHoleType: getB2GHoleType(item),
                B2GWelded: getB2GWelded(item),
                G2BPriority: getG2BPriority(item),
                G2BDetails: getG2BDetails(item),
                G2BConnectionMethod: getG2BConnectionMethod(item),
                G2BBolted: getG2BBolted(item),
                G2BHoleType: getG2BHoleType(item),
                G2BConnectionParam: getG2BConnectionParameter(item),
                G2BWelded: getG2BWelded(item),
                G2BCope: getG2BCope(item)

            }
            data.push(row);

        });
    }
    return data;
}

function getImeprialFromMM(Length_MM) {
    var MMValue = 25.4;
    var floatingPointPart = (Length_MM / MMValue) % 1;
    var integerPart = parseInt(Length_MM / MMValue);

    var numerator = Math.round(floatingPointPart * 16);
    var denominator = 16;
    var fractionText = "0";

    if (numerator > 0) {
        while (numerator % 2 == 0 && denominator % 2 == 0) {
            numerator /= 2;
            denominator /= 2;
        }

        if (numerator == 1 && denominator == 1) {
            integerPart++;

        }
        if (denominator > 1 && numerator >= 1) {
            fractionText = numerator + "/" + denominator;
        }
    }
    // Feet Inch Caluclation

    var feet = Math.floor(integerPart / 12);
    var ftProd_floatpart = parseFloat(integerPart / 12 % 1).toFixed(2);

    var inches = Math.round(ftProd_floatpart * 12);
    var feetInchText = "";

    if (feet > 0) {
        feetInchText = feet.toString();
    }

    //::feet start
    //feet && inches && fractionText
    if (feetInchText && inches && fractionText != '0') {
        feetInchText = "".concat(feet, "' ", "- ", inches, '" ', fractionText)
    }

    //feet && inches !=0 
    if (feetInchText && (!inches || inches) && fractionText == '0') {
        feetInchText = "".concat(feet, "' ", "- ", inches, '"')
    }
    //::feet end 

    //inches && fractionText != 0
    if (!feetInchText && inches && (fractionText && fractionText != '0')) {
        feetInchText = "".concat(inches, '" ', fractionText)
    }

    //inches & fractionText == 0
    if (!feetInchText && inches && (fractionText && fractionText == '0')) {
        feetInchText = "".concat(inches, '"')
    }

    //fractionText
    if (!feetInchText && !inches && fractionText) {
        feetInchText = "".concat(fractionText)
    }

    return feetInchText;
}

function getBraceProfile(item) {

    if (item.ConnectionType == "3") {

        var braceRows = { rows: [] };
        braceRows.rows.push(getBraceProfileDetails(item.Brace1toGusset));
        braceRows.rows.push(getBraceProfileDetails(item.Brace2toGusset));
        return braceRows;

    } else {
        return getBraceProfileDetails(item.Brace1toGusset)
    }
}
function getBraceProfileDetails(BracetoGusset) {

    switch (BracetoGusset.BraceShape) {
        case "L":
            return BracetoGusset.Angle.Profile;
        case "HSS":
            return BracetoGusset.HSS.Profile;
        case "W":
            return BracetoGusset.W.Profile;
        case "WT":
            return BracetoGusset.WT.Profile;
    }
}

function getGussetDetails(item) {
    return {
        Thickness: item.Gusset.Thickness ? getImeprialFromMM(item.Gusset.Thickness.mm) : '',
        Grade: item.Gusset.Grade ? item.Gusset.Grade : ''

    }
}

function getB2GDetails(item) {

    if (item.ConnectionType == "3") {

        var B2gBraceDetailRows = { rows: [] };
        B2gBraceDetailRows.rows.push(getB2GBraceDetails(item.Brace1toGusset));
        B2gBraceDetailRows.rows.push(getB2GBraceDetails(item.Brace2toGusset));
        return B2gBraceDetailRows;

    } else {
        return getB2GBraceDetails(item.Brace1toGusset)
    }

}

function getB2GBraceDetails(BracetoGusset) {
    var B2GDetails = [];
    switch (BracetoGusset.BraceShape) {
        case "L":
            B2GDetails.push({
                key: "Type",
                value: BracetoGusset.Angle.Type
            });
            if (BracetoGusset.Angle.Type == "DOUBLE") {
                B2GDetails.push({
                    key: "Angle Back to Back",
                    value: BracetoGusset.Angle.AngleBackToBack
                });
            }
            break;

        case "HSS":
            if (BracetoGusset.HSS.ConnectionType == "DIRECTLY_WELDED") {
                B2GDetails.push({
                    key: "Method",
                    value: BracetoGusset.HSS.DirectlyWelded.ConnectionMethod
                });
            }
            else if (BracetoGusset.HSS.ConnectionType == "KNIFE_PLATE") {
                B2GDetails.push({
                    key: "Set Back",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.SetBack.mm)
                });
            }
            break;

        case "W":
            B2GDetails.push({
                key: "Brace Orientation",
                value: BracetoGusset.W.WebOrientation
            });
            B2GDetails.push({
                key: "Set Back",
                value: getImeprialFromMM(BracetoGusset.W.SetBack.mm)
            });

            break;

        case "WT":
            B2GDetails.push({
                key: "Toe Type",
                value: BracetoGusset.WT.ToeType
            });
            if (BracetoGusset.WT.ToeType == "HORIZONTAL") {
                B2GDetails.push({
                    key: "Flange Cut Clearance",
                    value: getImeprialFromMM(BracetoGusset.WT.FlangeCutClearance.mm)
                });
            }
            break;

    }
    if (BracetoGusset.MaximumEccentricity.Required) {
        B2GDetails.push({
            key: "Maximum Eccentricity",
            value: getImeprialFromMM(BracetoGusset.MaximumEccentricity.Value.mm)
        });
    }
    return B2GDetails;
}

function getB2GBraceConnectionMetod(item) {

    if (item.ConnectionType == "3") {
        var braceRows = { rows: [] };
        braceRows.rows.push(getB2GBraceConnectionMetodDetails(item.Brace1toGusset));
        braceRows.rows.push(getB2GBraceConnectionMetodDetails(item.Brace2toGusset));
        return braceRows;


    } else {
        return getB2GBraceConnectionMetodDetails(item.Brace1toGusset)
    }

}

function getB2GBraceConnectionMetodDetails(BracetoGusset) {

    var B2GConnMethod = [];
    switch (BracetoGusset.BraceShape) {
        case "L":
            return BracetoGusset.Angle.ConnectionMethod;

        case "HSS":
            var hss = [];
            hss.push({
                key: "Type",
                value: BracetoGusset.HSS.ConnectionType
            });
            if (BracetoGusset.HSS.ConnectionType == "KNIFE_PLATE") {
                hss.push({
                    key: "Thickness",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.Thickness.mm)
                });
                hss.push({
                    key: "Grade",
                    value: BracetoGusset.HSS.KnifePlate.Grade
                });
                hss.push({
                    key: "Support",
                    value: BracetoGusset.HSS.KnifePlate.SupportSide
                });
                hss.push({
                    key: "Supported",
                    value: BracetoGusset.HSS.KnifePlate.SupportedSide
                });
            }

            B2GConnMethod.push({ rows: hss });
            return B2GConnMethod;


        case "W":
            var Web = [];
            Web.push({
                key: "Web",
                value: BracetoGusset.W.ConnectionType.Web.Details.Type
            });
            if (BracetoGusset.W.WebOrientation == "VERTICAL") {
                Web.push({
                    key: "Profile",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.Profile
                });
                Web.push({
                    key: "Grade",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.Profile

                });
                Web.push({
                    key: "AngleOSL",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.AngleOSL
                });
            }
            else {
                Web.push({
                    key: "Type",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.Type
                });
                Web.push({
                    key: "Plate Thickness",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.Thickness.mm)

                });
                Web.push({
                    key: "Grade",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.Grade

                });
            }
            Web.push({
                key: "Support",
                value: BracetoGusset.W.ConnectionType.Web.Details.SupportSide
            });
            Web.push({
                key: "Supported",
                value: BracetoGusset.W.ConnectionType.Web.Details.SupportedSide
            });

            B2GConnMethod.push({ rows: Web });

            if (BracetoGusset.W.ConnectionType.Flange.Required == true) {

                var Flange = [];
                Flange.push({
                    key: "Flange",
                    value: BracetoGusset.W.ConnectionType.Flange.Details.Type
                });
                Flange.push({
                    key: "Support",
                    value: BracetoGusset.W.ConnectionType.Flange.Details.SupportSide
                });
                Flange.push({
                    key: "Supported",
                    value: BracetoGusset.W.ConnectionType.Flange.Details.SupportedSide
                });

                Flange.push({
                    key: "Profile",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.Profile
                });

                Flange.push({
                    key: "Grade",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.Grade
                });

                Flange.push({
                    key: "Angle OSL",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.AngleOSL
                });


                B2GConnMethod.push({ rows: Flange });
            }
            return B2GConnMethod;

        case "WT":
            return BracetoGusset.WT.ConnectionMethod

    }

}

function getB2GBBolted(item) {

    if (item.ConnectionType == "3") {
        var braceRows = { rows: [] };
        braceRows.rows.push(getB2GBBoltedDetails(item.Brace1toGusset));
        braceRows.rows.push(getB2GBBoltedDetails(item.Brace2toGusset));
        return braceRows;


    } else {
        return getB2GBBoltedDetails(item.Brace1toGusset)
    }
}

function getB2GBBoltedDetails(BracetoGusset) {

    var B2GBolted = [];
    switch (BracetoGusset.BraceShape) {
        case "L":

            if (BracetoGusset.Angle.ConnectionMethod.includes("BOLTED")) {
                var nameCol = [];
                nameCol.push({
                    key: "Name",
                    value: BracetoGusset.Angle.BoltDetails.BoltName
                });
                nameCol.push({
                    key: "Grade",
                    value: BracetoGusset.Angle.BoltDetails.BoGr
                });
                nameCol.push({
                    key: "Dia",
                    value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltDia.mm)
                });

                B2GBolted.push({ cols: nameCol });

                if (BracetoGusset.Angle.BoltDetails.BoltSpacingRow.min_max_s == true) {
                    var minmaxcol = [];
                    minmaxcol.push({
                        key: "Min",
                        value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingRow.Minimum.mm)
                    });
                    minmaxcol.push({
                        key: "Max",
                        value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingRow.Maximum.mm)
                    });
                    minmaxcol.push({
                        key: "Increment",
                        value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingRow.Increment.mm)
                    });
                    B2GBolted.push({ cols: minmaxcol });
                }


                var rowcol = [];
                rowcol.push({
                    key: "No. of Rows",
                    value: BracetoGusset.Angle.BoltDetails.NoofBoltRows.N
                });
                rowcol.push({
                    key: "Row Spacing",
                    value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingRow.Default.mm)
                });
                rowcol.push({
                    key: "No. of Column",
                    value: BracetoGusset.Angle.BoltDetails.NoofBoltColumns.N_C
                });

                if (BracetoGusset.Angle.Type == "SINGLE" ||
                    (BracetoGusset.Angle.Type == "DOUBLE" && BracetoGusset.Angle.AngleBackToBack == "HORIZONTAL")) {
                    if (BracetoGusset.Angle.BoltDetails.NoofBoltColumns.N_C == 1) {

                        if (BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.Value.mm)
                            });
                        }
                        else {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage
                            });
                        }
                    }
                    if (BracetoGusset.Angle.BoltDetails.NoofBoltColumns.N_C == 2) {

                        if (BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.Value.mm)
                            });
                        } else {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage
                            });
                        }

                        if (BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn2.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "2 Column Spcacing",
                                value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn2.Value.mm)
                            });
                        }
                        else {

                            rowcol.push({
                                key: "2 Column Spcacing",
                                value: BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn2.BoltGage
                            });
                        }
                    }

                }
                else if (BracetoGusset.Angle.Type == "DOUBLE" && BracetoGusset.Angle.AngleBackToBack == "VERTICAL") {


                    if (BracetoGusset.Angle.BoltDetails.NoofBoltColumns.N_C == 2) {
                        if (BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "Column Spacing",
                                value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.CenterToCenterValue.mm)
                            });
                        }
                        else {
                            rowcol.push({
                                key: "Column Spacing",
                                value: BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.BoltGage
                            });
                        }

                    }

                    if (BracetoGusset.Angle.BoltDetails.NoofBoltColumns.N_C == 4) {
                        if (BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "Column Spacing",
                                value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.CenterToCenterValue.mm)
                            });
                        }
                        else {
                            rowcol.push({
                                key: "Column Spacing",
                                value: BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.BoltGage
                            });
                        }

                        if (BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.Value.mm)
                            });
                        } else {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: BracetoGusset.Angle.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage
                            });
                        }

                    }

                }
                B2GBolted.push({ cols: rowcol });
            }
            break;


        case "HSS":
            if (BracetoGusset.HSS.ConnectionType == "DIRECTLY_WELDED") {
                var nameCol = [];
                nameCol.push({
                    key: "Name",
                    value: BracetoGusset.HSS.DirectlyWelded.ErectionBolt.BoltName
                });
                nameCol.push({
                    key: "Grade",
                    value: BracetoGusset.HSS.DirectlyWelded.ErectionBolt.BoGr
                });
                nameCol.push({
                    key: "Dia",
                    value: getImeprialFromMM(BracetoGusset.HSS.DirectlyWelded.ErectionBolt.BoltDia.mm)
                });

                B2GBolted.push({ cols: nameCol });
            }
            else if (BracetoGusset.HSS.ConnectionType == "KNIFE_PLATE") {
                var nameCol = [];
                nameCol.push({
                    key: "Name",
                    value: BracetoGusset.HSS.KnifePlate.BoltDetails.BoltName
                });
                nameCol.push({
                    key: "Grade",
                    value: BracetoGusset.HSS.KnifePlate.BoltDetails.BoGr
                });
                nameCol.push({
                    key: "Dia",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.BoltDia.mm)
                });

                B2GBolted.push({ cols: nameCol });

                if (BracetoGusset.HSS.KnifePlate.BoltDetails.BoltSpacingRows.min_max_s == true) {
                    var minmaxcol = [];
                    minmaxcol.push({
                        key: "Min",
                        value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.BoltSpacingRows.Minimum.mm)
                    });
                    minmaxcol.push({
                        key: "Max",
                        value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.BoltSpacingRows.Maximum.mm)
                    });
                    minmaxcol.push({
                        key: "Increment",
                        value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.BoltSpacingRows.Increment.mm)
                    });
                    B2GBolted.push({ cols: minmaxcol });
                }

                var rowcol = [];
                rowcol.push({
                    key: "No. of Rows",
                    value: BracetoGusset.HSS.KnifePlate.BoltDetails.NoofBoltRows.N
                });
                rowcol.push({
                    key: "Row Spacing",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.BoltSpacingRows.Default.mm)
                });
                rowcol.push({
                    key: "No. of Column",
                    value: BracetoGusset.HSS.KnifePlate.BoltDetails.NoofBoltColumns.N_C
                });
                rowcol.push({
                    key: "Column Spacing",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.BoltSpacingColumns.Default.mm)
                });
                B2GBolted.push({ cols: rowcol });

            }
            break;
        case "W":

            if (BracetoGusset.W.WebOrientation == "VERTICAL") {
                var nameCol = [];
                nameCol.push({
                    key: "Name",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.BoltName
                });
                nameCol.push({
                    key: "Grade",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.BoGr
                });
                nameCol.push({
                    key: "Dia",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.BoltDia.mm)
                });
                B2GBolted.push({ cols: nameCol });

                if (BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.min_max_s == true) {
                    var minmaxcol = [];
                    minmaxcol.push({
                        key: "Min",
                        value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.Minimum.mm)
                    });
                    minmaxcol.push({
                        key: "Max",
                        value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.Maximum.mm)
                    });
                    minmaxcol.push({
                        key: "Increment",
                        value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.Increment.mm)
                    });
                    B2GBolted.push({ cols: minmaxcol });
                }

                var rowcol = [];
                rowcol.push({
                    key: "No. of Rows",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.NoofBoltRows.N
                });
                rowcol.push({
                    key: "Row Spacing",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.Default.mm)
                });

                rowcol.push({
                    key: "Support Gage",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.GageDetails.Support.BoltGage)
                });
                if (BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.GageDetails.Support.BoltGage == "SPECIFIC") {

                    rowcol.push({
                        key: "Support Value",
                        value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.GageDetails.Supported.Value.mm)
                    });

                }
                rowcol.push({
                    key: "Supported Gage",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.GageDetails.Support.BoltGage
                });
                if (BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.GageDetails.Support.BoltGage == "SPECIFIC") {
                    rowcol.push({
                        key: "Supported Value",
                        value: BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.GageDetails.Supported.BoltGage
                    });

                }
                B2GBolted.push({ cols: rowcol });

            }
            else {

                var nameCol = { rows: [] };
                var webNameCol = [];
                webNameCol.push({
                    key: "Name",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoltName
                });
                webNameCol.push({
                    key: "Grade",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoGr
                });
                webNameCol.push({
                    key: "Dia",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoltDia.mm)
                });
                nameCol.rows.push({ data: webNameCol });

                var flangeNameCol = [];
                flangeNameCol.push({
                    key: "Name",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.BoltName
                });
                flangeNameCol.push({
                    key: "Grade",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.BoGr
                });
                flangeNameCol.push({
                    key: "Dia",
                    value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.BoltDia.mm)
                });
                nameCol.rows.push({ data: flangeNameCol });

                B2GBolted.push({ cols: nameCol });


                var minmaxcol = { rows: [] };
                if (BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoltSpacingRows.min_max_s == true) {

                    var webMinmaxcol = [];
                    webMinmaxcol.push({
                        key: "Min",
                        value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoltSpacingRows.Minimum.mm)
                    });
                    webMinmaxcol.push({
                        key: "Max",
                        value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoltSpacingRows.Maximum.mm)
                    });
                    webMinmaxcol.push({
                        key: "Increment",
                        value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoltSpacingRows.Increment.mm)
                    });
                    minmaxcol.rows.push({ data: webMinmaxcol });
                }
                if (BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.min_max_s == true) {

                    var flangeMinmaxcol = [];
                    flangeMinmaxcol.push({
                        key: "Min",
                        value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.Minimum.mm)
                    });
                    flangeMinmaxcol.push({
                        key: "Max",
                        value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.Maximum.mm)
                    });
                    flangeMinmaxcol.push({
                        key: "Increment",
                        value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.Increment.mm)
                    });
                    minmaxcol.rows.push({ data: flangeMinmaxcol });
                }
                B2GBolted.push({ cols: minmaxcol });

                var rowcol = { rows: [] };
                var webRowcol = [];
                webRowcol.push({
                    key: "No. of Rows",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.NoofBoltRows.N
                });

                if (BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.NoofBoltColumns.N_C > 1) {
                    webRowcol.push({
                        key: "Row Spacing",
                        value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoltSpacingRows.Default.mm)
                    });
                }
                webRowcol.push({
                    key: "No. of Column",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.NoofBoltColumns.N_C
                });
                webRowcol.push({
                    key: "Column Spacing",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.BoltSpacingColumns.Default.mm)
                });
                rowcol.rows.push({ data: webRowcol });

                var flangeRowcol = [];
                flangeRowcol.push({
                    key: "Support Gage",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.GageDetails.Support.BoltGage
                });
                if (BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.GageDetails.Support.BoltGage == "SPECIFIC") {
                    flangeRowcol.push({
                        key: "Support Value",
                        value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.GageDetails.Support.Value.mm)
                    });
                }
                flangeRowcol.push({
                    key: "Supported Gage",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.GageDetails.Supported.BoltGage
                });
                if (BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.GageDetails.Supported.BoltGage == "SPECIFIC") {

                    flangeRowcol.push({
                        key: "Supported Value",
                        value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.GageDetails.Supported.Value.mm)
                    });
                }
                flangeRowcol.push({
                    key: "No. of Rows",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.NoofBoltRows.N
                });
                flangeRowcol.push({
                    key: "Row Spacing",
                    value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.BoltSpacingRows.Default.mm)
                });

                rowcol.rows.push({ data: flangeRowcol });

                B2GBolted.push({ cols: rowcol });

            }
            break;

        case "WT":
            if (BracetoGusset.WT.ConnectionMethod.includes("BOLTED")) {
                var nameCol = [];
                nameCol.push({
                    key: "Name",
                    value: BracetoGusset.WT.BoltDetails.BoltName
                });
                nameCol.push({
                    key: "Grade",
                    value: BracetoGusset.WT.BoltDetails.BoGr
                });
                nameCol.push({
                    key: "Dia",
                    value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltDia.mm)
                });
                B2GBolted.push({ cols: nameCol });

                if (BracetoGusset.WT.BoltDetails.BoltSpacingRow.min_max_s == true) {
                    var minmaxcol = [];
                    minmaxcol.push({
                        key: "Min",
                        value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingRow.Minimum.mm)
                    });
                    minmaxcol.push({
                        key: "Max",
                        value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingRow.Maximum.mm)
                    });
                    minmaxcol.push({
                        key: "Increment",
                        value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingRow.Increment.mm)
                    });
                    B2GBolted.push({ cols: minmaxcol });
                }

                var rowcol = [];
                rowcol.push({
                    key: "No. of Rows",
                    value: BracetoGusset.WT.BoltDetails.NoofBoltRows.N
                });
                rowcol.push({
                    key: "Row Spacing",
                    value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingRow.Default.mm)
                });
                rowcol.push({
                    key: "No. of Column",
                    value: BracetoGusset.WT.BoltDetails.NoofBoltColumns.N_C
                });


                if (BracetoGusset.WT.ToeType == "VERTICAL") {
                    if (BracetoGusset.WT.BoltDetails.NoofBoltColumns.N_C == 2) {

                        if (BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "Column Spacing",
                                value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.CenterToCenterValue.mm)
                            });
                        }
                        else {
                            rowcol.push({
                                key: "Column Spacing",
                                value: BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.BoltGage
                            });
                        }

                    }
                    else if (BracetoGusset.WT.BoltDetails.NoofBoltColumns.N_C == 4) {

                        if (BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "Column Spacing",
                                value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.CenterToCenterValue.mm)
                            });
                        }
                        else {
                            rowcol.push({
                                key: "Column Spacing",
                                value: BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltCenterToCenter.BoltGage
                            });
                        }

                        if (BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.Value.mm)
                            });
                        } else {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage
                            });
                        }

                    }
                }
                else if (BracetoGusset.WT.ToeType == "HORIZONTAL") {

                    if (BracetoGusset.WT.BoltDetails.NoofBoltColumns.N_C == 1) {

                        if (BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.Value.mm)
                            });
                        } else {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage
                            });
                        }

                    }
                    else if (BracetoGusset.WT.BoltDetails.NoofBoltColumns.N_C == 4) {
                        if (BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.Value.mm)
                            });
                        } else {
                            rowcol.push({
                                key: "1 Column Spcacing",
                                value: BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn1.BoltGage
                            });
                        }

                        if (BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn2.BoltGage == "SPECIFIC") {
                            rowcol.push({
                                key: "2 Column Spcacing",
                                value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn2.Value.mm)
                            });
                        }
                        else {

                            rowcol.push({
                                key: "2 Column Spcacing",
                                value: BracetoGusset.WT.BoltDetails.BoltSpacingColumn.BoltColumn2.BoltGage
                            });
                        }

                    }

                }

                B2GBolted.push({ cols: rowcol });

            }
            break;
    }
    return B2GBolted;
}

function getB2GEdgeDistance(item) {

    if (item.ConnectionType == "3") {
        var braceRows = { rows: [] };
        braceRows.rows.push(getB2GEdgeDistanceDetails(item.Brace1toGusset));
        braceRows.rows.push(getB2GEdgeDistanceDetails(item.Brace2toGusset));
        return braceRows;


    } else {
        return getB2GEdgeDistanceDetails(item.Brace1toGusset)
    }
}

function getB2GEdgeDistanceDetails(BracetoGusset) {

    switch (BracetoGusset.BraceShape) {
        case "L":
            var B2GEdgeDistCol = [];
            if (BracetoGusset.Angle.ConnectionMethod.includes("BOLTED")) {
                B2GEdgeDistCol.push({
                    key: "Gusset",
                    value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.EdgeDistance.Gusset.mm)
                });
                B2GEdgeDistCol.push({
                    key: "Brace",
                    value: getImeprialFromMM(BracetoGusset.Angle.BoltDetails.EdgeDistance.Brace.mm)
                });
            }
            return B2GEdgeDistCol;

        case "HSS":
            var B2GEdgeDistCol = [];
            if (BracetoGusset.HSS.ConnectionMethod == "KNIFE_PLATE") {

                B2GEdgeDistCol.push({
                    key: "Gusset",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.EdgeDistance.Gusset.mm)
                });
                B2GEdgeDistCol.push({
                    key: "Plate Horizontal",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.EdgeDistance.KnifePlate.Horizontal.mm)
                });
                B2GEdgeDistCol.push({
                    key: "Plate Vertical",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.BoltDetails.EdgeDistance.KnifePlate.Vertical.mm)
                });
            }
            return B2GEdgeDistCol;
        case "W":

            if (BracetoGusset.W.WebOrientation == "VERTICAL") {
                var B2GEdgeDistCol = [];
                B2GEdgeDistCol.push({
                    key: "Gusset",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.EdgeDistance.Gusset.mm)
                });
                B2GEdgeDistCol.push({
                    key: "Brace",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.EdgeDistance.Brace.mm)
                });
                B2GEdgeDistCol.push({
                    key: "Claw Angle",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.EdgeDistance.ClawAngle.mm)
                });
                return B2GEdgeDistCol;
            }
            else {
                var B2GEdgeDistRows = { rows: [] };
                var WebEdgeDist = [];
                WebEdgeDist.push({
                    key: "Gusset",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.EdgeDistance.Gusset.mm)
                });
                WebEdgeDist.push({
                    key: "Brace",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.EdgeDistance.Brace.mm)
                });
                WebEdgeDist.push({
                    key: "Plate Horizontal",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.EdgeDistance.SplicePlate.Horizontal.mm)
                });
                WebEdgeDist.push({
                    key: "Plate Vertical",
                    value: getImeprialFromMM(BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.EdgeDistance.SplicePlate.Vertical.mm)
                });
                B2GEdgeDistRows.rows.push(WebEdgeDist);

                //for Flange

                var FlangeEdgeDist = [];
                FlangeEdgeDist.push({
                    key: "Gusset",
                    value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.EdgeDistance.Gusset.mm)
                });
                FlangeEdgeDist.push({
                    key: "Brace",
                    value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.EdgeDistance.Brace.mm)
                });
                FlangeEdgeDist.push({
                    key: "Claw Angle",
                    value: getImeprialFromMM(BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.EdgeDistance.ClawAngle.mm)
                });

                B2GEdgeDistRows.rows.push(FlangeEdgeDist);
                return B2GEdgeDistRows;

            }


        case "WT":
            var B2GEdgeDistCol = [];
            if (BracetoGusset.WT.ConnectionMethod.includes("BOLTED")) {
                B2GEdgeDistCol.push({
                    key: "Gusset",
                    value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.EdgeDistance.Gusset.mm)
                });
                B2GEdgeDistCol.push({
                    key: "Brace",
                    value: getImeprialFromMM(BracetoGusset.WT.BoltDetails.EdgeDistance.Brace.mm)
                });
            }
            return B2GEdgeDistCol;

    }
}

function getB2GHoleType(item) {

    if (item.ConnectionType == "3") {
        var braceRows = { rows: [] };
        braceRows.rows.push(getB2GHoleTypeDetails(item.Brace1toGusset));
        braceRows.rows.push(getB2GHoleTypeDetails(item.Brace2toGusset));
        return braceRows;


    } else {
        return getB2GHoleTypeDetails(item.Brace1toGusset)
    }

}

function getB2GHoleTypeDetails(BracetoGusset) {


    switch (BracetoGusset.BraceShape) {
        case "L":
            var B2GHoleType = [];
            if (BracetoGusset.Angle.ConnectionMethod.includes("BOLTED")) {
                B2GHoleType.push({
                    key: "Gusset",
                    value: BracetoGusset.Angle.BoltDetails.HoleType.Gusset.dhh
                });
                B2GHoleType.push({
                    key: "Brace",
                    value: BracetoGusset.Angle.BoltDetails.HoleType.Brace.dhh
                });
            }
            return B2GHoleType;
        case "HSS":
            var B2GHoleType = [];
            if (BracetoGusset.HSS.ConnectionMethod == "KNIFE_PLATE") {
                B2GHoleType.push({
                    key: "Gusset",
                    value: BracetoGusset.HSS.KnifePlate.BoltDetails.HoleType.Gusset.dhh
                });
                B2GHoleType.push({
                    key: "Plate",
                    value: BracetoGusset.HSS.KnifePlate.BoltDetails.HoleType.KnifePlate.dhh
                });
            }
            return B2GHoleType;
        case "W":

            if (BracetoGusset.W.WebOrientation == "VERTICAL") {
                var B2GHoleType = [];
                B2GHoleType.push({
                    key: "Gusset",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.HoleType.Gusset.dhh
                });
                B2GHoleType.push({
                    key: "Brace",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.HoleType.Brace.dhh
                });
                B2GHoleType.push({
                    key: "Claw Angle",
                    value: BracetoGusset.W.WebConectionDetails.ClawAngle.BoltDetails.HoleType.ClawAngle.dhh
                });
                return B2GHoleType;

            }
            else {

                var B2GHoleTypeRows = { rows: [] };

                var B2GHoleTypeWeb = [];
                B2GHoleTypeWeb.push({
                    key: "Gusset",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.HoleType.Gusset.dhh
                });
                B2GHoleTypeWeb.push({
                    key: "Brace",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.HoleType.Brace.dhh
                });
                B2GHoleTypeWeb.push({
                    key: "Plate",
                    value: BracetoGusset.W.WebConectionDetails.SplicePlate.BoltDetails.HoleType.SplicePlate.dhh
                });
                B2GHoleTypeRows.rows.push(B2GHoleTypeWeb);

                //For Flange
                var B2GHoleTypeFlange = [];
                B2GHoleTypeFlange.push({
                    key: "Gusset",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.HoleType.Gusset.dhh
                });
                B2GHoleTypeFlange.push({
                    key: "Brace",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.HoleType.Brace.dhh
                });
                B2GHoleTypeFlange.push({
                    key: "Claw Angle",
                    value: BracetoGusset.W.FlangeConectionDetails.ClawAngle.BoltDetails.HoleType.ClawAngle.dhh
                });
                B2GHoleTypeRows.rows.push(B2GHoleTypeFlange);
                return B2GHoleTypeRows;
            }
        case "WT":
            var B2GHoleType = [];
            if (BracetoGusset.WT.ConnectionMethod.includes("BOLTED")) {
                B2GHoleType.push({
                    key: "Gusset",
                    value: BracetoGusset.WT.BoltDetails.HoleType.Gusset.dhh
                });
                B2GHoleType.push({
                    key: "Brace",
                    value: BracetoGusset.WT.BoltDetails.HoleType.Brace.dhh
                });
            }
            return B2GHoleType;

    }

}

function getB2GWelded(item) {

    if (item.ConnectionType == "3") {
        var braceRows = { rows: [] };
        braceRows.rows.push(getB2GWeldedDetails(item.Brace1toGusset));
        braceRows.rows.push(getB2GWeldedDetails(item.Brace2toGusset));
        return braceRows;


    } else {
        return getB2GWeldedDetails(item.Brace1toGusset)
    }

}

function getB2GWeldedDetails(BracetoGusset) {

    var B2GWelded = [];
    switch (BracetoGusset.BraceShape) {
        case "L":
            if (!BracetoGusset.Angle.ConnectionMethod.includes("BOLTED")) {
                B2GWelded.push({
                    key: "Type",
                    value: BracetoGusset.Angle.WeldDetails.wtype
                });
                B2GWelded.push({
                    key: "Size",
                    value: getImeprialFromMM(BracetoGusset.Angle.WeldDetails.WeldSize.mm)
                });
                B2GWelded.push({
                    key: "Length",
                    value: getImeprialFromMM(BracetoGusset.Angle.WeldDetails.WeldLength.mm)
                });
                B2GWelded.push({
                    key: "Tail info",
                    value: BracetoGusset.Angle.WeldDetails.TailInfo
                });
                B2GWelded.push({
                    key: "Seal Weld",
                    value: BracetoGusset.Angle.WeldDetails.SealWeld
                });
            }
            break;
        case "HSS":
            if (BracetoGusset.HSS.ConnectionMethod == "KNIFE_PLATE") {
                B2GWelded.push({
                    key: "Type",
                    value: BracetoGusset.HSS.KnifePlate.WeldDetails.wtype
                });
                B2GWelded.push({
                    key: "Size",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.WeldDetails.WeldSize.mm)
                });
                B2GWelded.push({
                    key: "Length",
                    value: getImeprialFromMM(BracetoGusset.HSS.KnifePlate.WeldDetails.WeldLength.mm)
                });
                B2GWelded.push({
                    key: "Tail info",
                    value: BracetoGusset.HSS.KnifePlate.WeldDetails.TailInfo
                });
            }
            else if (BracetoGusset.HSS.ConnectionMethod == "DIRECTLY_WELDED") {
                B2GWelded.push({
                    key: "Type",
                    value: BracetoGusset.HSS.DirectlyWelded.wtype
                });
                B2GWelded.push({
                    key: "Size",
                    value: getImeprialFromMM(BracetoGusset.HSS.DirectlyWelded.WeldSize.mm)
                });
                B2GWelded.push({
                    key: "Length",
                    value: getImeprialFromMM(BracetoGusset.HSS.DirectlyWelded.WeldLength.mm)
                });
                B2GWelded.push({
                    key: "Tail info",
                    value: BracetoGusset.HSS.DirectlyWelded.TailInfo
                });
            }
            break;
        case "WT":
            if (!BracetoGusset.WT.ConnectionMethod.includes("BOLTED")) {
                B2GWelded.push({
                    key: "Type",
                    value: BracetoGusset.WT.WeldDetails.wtype
                });
                B2GWelded.push({
                    key: "Size",
                    value: getImeprialFromMM(BracetoGusset.WT.WeldDetails.WeldSize.mm)
                });
                B2GWelded.push({
                    key: "Length",
                    value: getImeprialFromMM(BracetoGusset.WT.WeldDetails.WeldLength.mm)
                });
                B2GWelded.push({
                    key: "Tail info",
                    value: BracetoGusset.WT.WeldDetails.TailInfo
                });
                B2GWelded.push({
                    key: "Seal Weld",
                    value: BracetoGusset.WT.WeldDetails.SealWeld
                });
            }

    }
    return B2GWelded;
}

// Gusset To Beam

function getG2BPriority(item) {

    var beamRows = [];
    beamRows = beamRows.concat(getG2BPriorityDetails(item.GussetToBeam.GussetToBeam1, item.GussetToBeam.Type, "Beam 1"));
    if (item.ConnectionType != "3" && item.ConnectionType != "4") {
        beamRows = beamRows.concat(getG2BPriorityDetails(item.GussetToBeam.GussetToBeam2, item.GussetToBeam.Type, "Beam 2"));
    }
    return beamRows;
}

function getG2BPriorityDetails(GussetToBeam, ConnectionType, Beam) {

    var rows = [];
    if (ConnectionType == "INDIVIDUAL_CONNECTION") {

        rows.push({ key: Beam, value: "\n " + GussetToBeam.Priority[0].P1 });

    } else if (ConnectionType == "GROUP_CONNECTION") {

        for (x in GussetToBeam.Priority) {

            JsonKey = "P" + (parseInt(x) + 1);
            if (GussetToBeam.Priority[x][JsonKey]) {

                rows.push({ key: JsonKey, value: GussetToBeam.Priority[x][JsonKey] });
                rows.push("");

            }

        }
    }
    return rows;
}

function getG2BDetails(item) {

    var beamRows = { rows: [] };
    if (item.GussetToBeam.Type == "INDIVIDUAL_CONNECTION") {

        if (item.GussetToBeam.GussetToBeam1.Priority[0].P1 == "Chevron") {
            beamRows.rows.push(getDetailsType3_4_Weld(item.GussetToBeam.GussetToBeam1.Chevron));
            beamRows.rows.push(getDetailsCheveronnterMediateStiffener(item.GussetToBeam.GussetToBeam1));
        }
        else if (item.GussetToBeam.GussetToBeam1.Priority[0].P1 == "KneeBrace") {
            beamRows.rows.push(getDetailsType3_4_Weld(item.GussetToBeam.GussetToBeam1.KneeBrace.KB));
            beamRows.rows.push(getDetailsKneeBraceWorkPoints(item.GussetToBeam.GussetToBeam1.KneeBrace));
        }

        else {
            beamRows.rows.push(getBeamDetails(item.GussetToBeam.GussetToBeam1, item.GussetToBeam.GussetToBeam1.Priority[0].P1));
            beamRows.rows.push(getBeamDetails(item.GussetToBeam.GussetToBeam2, item.GussetToBeam.GussetToBeam2.Priority[0].P1));

        }
    }
    else {
        for (x in item.GussetToBeam.GussetToBeam1.Priority) {
            var JsonKey = "P" + (parseInt(x) + 1);
            var type = item.GussetToBeam.GussetToBeam1.Priority[x][JsonKey];

            if (type == "Chevron") {
                beamRows.rows.push(getDetailsType3_4_Weld(item.GussetToBeam.GussetToBeam1.Chevron));
                beamRows.rows.push(getDetailsCheveronnterMediateStiffener(item.GussetToBeam.GussetToBeam1));

            }
            else if (type == "KneeBrace") {

                beamRows.rows.push(getDetailsType3_4_Weld(item.GussetToBeam.GussetToBeam1.KneeBrace.KB));
                beamRows.rows.push(getDetailsKneeBraceWorkPoints(item.GussetToBeam.GussetToBeam1.KneeBrace));
            }
            else {
                beamRows.rows.push(getBeamDetails(item.GussetToBeam.GussetToBeam1, type));
                beamRows.rows.push([]);
            }

        }
    }
    return beamRows;
}

function getDetailsKneeBraceWorkPoints(obj) {

    var beamRows = [];
    beamRows.push({
        key: "Work Point",
        value: obj.KB.Details.WorkpointDetails.Type
    });
    if (obj.KB.Details.WorkpointDetails.Type == "INSIDE_GUSSET") {
        beamRows.push({
            key: "L1",
            value: getImeprialFromMM(obj.KB.Details.WorkpointDetails.InsideGusset.L1.mm)
        });
        beamRows.push({
            key: "L2",
            value: getImeprialFromMM(obj.KB.Details.WorkpointDetails.InsideGusset.L2.mm)
        });

    } else if (obj.KB.Details.WorkpointDetails.Type == "OUTSIDE_GUSSET") {
        beamRows.push({
            key: "L1",
            value: getImeprialFromMM(obj.KB.Details.WorkpointDetails.OutsideGusset.L1.mm)
        });
        beamRows.push({
            key: "L2",
            value: getImeprialFromMM(obj.KB.Details.WorkpointDetails.OutsideGusset.L2.mm)
        });
    }
    return beamRows;
}
function getDetailsCheveronnterMediateStiffener(GussetToBeam) {

    var beamRows = [];
    if (GussetToBeam.Chevron.Details.Stiffeners.IntermidiateStiffeners.Required) {
        beamRows.push({
            key: "Intermediate Stiffener",
            value: "Required"

        },
            {
                key: "No of Stiffeners",
                value: GussetToBeam.Chevron.Details.Stiffeners.IntermidiateStiffeners.NumberOfStiffeners

            },
            {
                key: "Type",
                value: GussetToBeam.Chevron.Details.Stiffeners.IntermidiateStiffeners.StiffenerType

            }
        );
    }
    return beamRows;
}
function getDetailsType3_4_Weld(obj) {

    var beamRows = [];
    beamRows.push({
        key: "Type",
        value: obj.Details.WeldDetails.wtype

    },
        {
            key: "Gusset Weld Length",
            value: getImeprialFromMM(obj.Details.WeldDetails.WeldLength.mm)

        },
        {
            key: "Weld Size",
            value: getImeprialFromMM(obj.Details.WeldDetails.WeldSize.mm)

        }
    );
    return beamRows;

}

function getBeamDetails(GussetToBeam, ConnectionType) {

    var beamRows = [];

    switch (ConnectionType) {

        case "SingleClipAngle":
            beamRows.push({
                key: "Profile",
                value: GussetToBeam.SingleClipAngle.Details.Profile

            },
                {
                    key: "Grade",
                    value: GussetToBeam.SingleClipAngle.Details.Grade

                },
                {
                    key: "OSL",
                    value: GussetToBeam.SingleClipAngle.Details.ClipAngleOSL

                },
                {
                    key: "Set Back",
                    value: getImeprialFromMM(GussetToBeam.SingleClipAngle.Details.Setback.mm)

                },
            );
            break;

        case "DoubleClipAngle":
            beamRows.push({
                key: "Profile",
                value: GussetToBeam.DoubleClipAngle.Details.Profile

            },
                {
                    key: "Grade",
                    value: GussetToBeam.DoubleClipAngle.Details.Grade

                },
                {
                    key: "OSL",
                    value: GussetToBeam.DoubleClipAngle.Details.ClipAngleOSL

                }
            );
            break;
        case "ShearPlate":
            beamRows.push({
                key: "Thickness",
                value: getImeprialFromMM(GussetToBeam.ShearPlate.Details.Thickness.mm)

            },
                {
                    key: "Grade",
                    value: GussetToBeam.ShearPlate.Details.Grade

                },
                {
                    key: "Set Back",
                    value: getImeprialFromMM(GussetToBeam.ShearPlate.Details.Setback.mm)

                }
            );
            break;

    }

    return beamRows;

}

function getG2BConnectionMethod(item) {

    var beamRows = { rows: [] };
    if (item.GussetToBeam.Type == "INDIVIDUAL_CONNECTION") {
        beamRows.rows = beamRows.rows.concat(getG2BConnectionMethodDetails(item.GussetToBeam.GussetToBeam1, item.GussetToBeam.GussetToBeam1.Priority[0].P1));
        beamRows.rows = beamRows.rows.concat(getG2BConnectionMethodDetails(item.GussetToBeam.GussetToBeam2, item.GussetToBeam.GussetToBeam2.Priority[0].P1));
    }
    else {
        for (x in item.GussetToBeam.GussetToBeam1.Priority) {
            var JsonKey = "P" + (parseInt(x) + 1);
            var type = item.GussetToBeam.GussetToBeam1.Priority[x][JsonKey];
            beamRows.rows = beamRows.rows.concat(getG2BConnectionMethodDetails(item.GussetToBeam.GussetToBeam1, type));
        }
    }
    return beamRows;
}

function getG2BConnectionMethodDetails(GussetToBeam, ConnectionType) {
    var beamRows = [];
    switch (ConnectionType) {
        case "SingleClipAngle":
            beamRows = beamRows.concat(getG2BConnectionMethodSupportSupportedvalues(
                GussetToBeam.SingleClipAngle.Details.SupportSide,
                GussetToBeam.SingleClipAngle.Details.SupportedSide
            ));
            break;
        case "DoubleClipAngle":
            beamRows = beamRows.concat(getG2BConnectionMethodSupportSupportedvalues(
                GussetToBeam.DoubleClipAngle.Details.SupportSide,
                GussetToBeam.DoubleClipAngle.Details.SupportedSide
            ));
            break;
        case "ShearPlate":
            beamRows = beamRows.concat(getG2BConnectionMethodSupportSupportedvalues(
                GussetToBeam.ShearPlate.Details.SupportSide,
                GussetToBeam.ShearPlate.Details.SupportedSide
            ));
            break;

            // Addig Empty key values to maitain allignment
            var values = [];
            values.push({ key: "", value: "" });
            values.push({ key: "", value: "" });
            beamRows = beamRows.concat(values);
            break;
        case "DirectlyBolted":
            var values = [];
            values.push({
                key: "",
                value:""

            });
            values.push({
                key: "",
                value: ""

            });
            beamRows = beamRows.concat(values);
            break;
        case "DirectlyWelded":
            var values = [];
            values.push({
                key: "Supported",
                value: GussetToBeam.DirectlyWelded.Details.WeldDetails.ConnectionMethod

            });
            // Addig Empty key values to maitain allignment
            values.push({
                key: "",
                value: ""

            });
            beamRows = beamRows.concat(values);
            break;
        case "Chevron":
            var values = [];
            values.push({
                key: "Connection Method",
                value: GussetToBeam.Chevron.Details.WeldDetails.ConnectionMethod

            });

            beamRows = beamRows.concat(values);
            break;
        case "KneeBrace":
            var values = [];
            values.push({
                key: "Connection Method",
                value: GussetToBeam.KneeBrace.KB.Details.WeldDetails.ConnectionMethod

            });

            beamRows = beamRows.concat(values);
    }

    return beamRows;

}

function getG2BConnectionMethodSupportSupportedvalues(supportValue, supportedValue) {

    var values = [];
    values.push({
        key: "Support",
        value: supportValue

    });
    values.push({
        key: "Supported",
        value: supportedValue

    });
    return values;
}

function getG2BBolted(item) {
    var beamRows = { rows: [] };
    if (item.GussetToBeam.Type == "INDIVIDUAL_CONNECTION") {

        beamRows.rows = beamRows.rows.concat(getG2BBoltDetails(item.GussetToBeam.GussetToBeam1, item.GussetToBeam.GussetToBeam1.Priority[0].P1));
        beamRows.rows = beamRows.rows.concat(getG2BBoltDetails(item.GussetToBeam.GussetToBeam2, item.GussetToBeam.GussetToBeam2.Priority[0].P1));
    }
    else {
        for (x in item.GussetToBeam.GussetToBeam1.Priority) {
            var JsonKey = "P" + (parseInt(x) + 1);
            var type = item.GussetToBeam.GussetToBeam1.Priority[x][JsonKey];
            beamRows.rows = beamRows.rows.concat(getG2BBoltDetails(item.GussetToBeam.GussetToBeam1, type));
        }

    }

    return beamRows;
}

function getG2BBoltDetails(GussetToBeam, ConnectionType) {

    var rows = [];
    switch (ConnectionType) {
        case "SingleClipAngle":
            if (GussetToBeam.SingleClipAngle.Details.SupportSide.includes("BOLTED")) {
                rows.push(getG2BBoltDetailsSupportSupportedValues(GussetToBeam.SingleClipAngle.Details.Support));

            }
            else {
                rows.push("");
            }

            if (GussetToBeam.SingleClipAngle.Details.SupportedSide.includes("BOLTED")) {
                rows.push(getG2BBoltDetailsSupportSupportedValues(GussetToBeam.SingleClipAngle.Details.Supported));

            }
            else {
                rows.push("");
            }
            break;
        case "DoubleClipAngle":
            if (GussetToBeam.DoubleClipAngle.Details.SupportSide.includes("BOLTED")) {
                rows.push(getG2BBoltDetailsSupportSupportedValues(GussetToBeam.DoubleClipAngle.Details.Support));

            }
            else {
                rows.push("");
            }

            if (GussetToBeam.DoubleClipAngle.Details.SupportedSide.includes("BOLTED")) {
                rows.push(getG2BBoltDetailsSupportSupportedValues(GussetToBeam.DoubleClipAngle.Details.Supported));

            }
            else {
                rows.push("");
            }
            break;
        case "ShearPlate":
            if (GussetToBeam.ShearPlate.Details.SupportSide.includes("BOLTED")) {
                rows.push(getG2BBoltDetailsSupportSupportedValues(GussetToBeam.ShearPlate.Details.Support));

            }
            else {
                rows.push("");
            }

            if (GussetToBeam.ShearPlate.Details.SupportedSide.includes("BOLTED")) {
                rows.push(getG2BBoltDetailsSupportSupportedValues(GussetToBeam.ShearPlate.Details.Supported));

            }
            else {
                rows.push("");
            }
            break;
        case "DirectlyBolted":
            rows.push(getG2BBoltDetailsSupportSupportedValues(GussetToBeam.DirectlyBolted.Details, true));
            rows.push("");
            break;
        case "DirectlyWelded":
            rows.push("");
            rows.push("");
            break;
    }
    return rows;
}

function getG2BBoltDetailsSupportSupportedValues(obj, gageRequired) {

    var B2GBolted = [];
    var nameCol = [];
    nameCol.push({
        key: "Name",
        value: obj.BoltDetails.BoltName
    });
    nameCol.push({
        key: "Grade",
        value: obj.BoltDetails.BoGr
    });
    nameCol.push({
        key: "Dia",
        value: getImeprialFromMM(obj.BoltDetails.BoltDia.mm)
    });

    B2GBolted.push({ cols: nameCol });

    if (obj.BoltDetails.BoltSpacingRows.min_max_s == true) {
        var minmaxcol = [];
        minmaxcol.push({
            key: "Min",
            value: getImeprialFromMM(obj.BoltDetails.BoltSpacingRows.Minimum.mm)
        });
        minmaxcol.push({
            key: "Max",
            value: getImeprialFromMM(obj.BoltDetails.BoltSpacingRows.Maximum.mm)
        });
        minmaxcol.push({
            key: "Increment",
            value: getImeprialFromMM(obj.BoltDetails.BoltSpacingRows.Increment.mm)
        });
        B2GBolted.push({ cols: minmaxcol });
    }


    var rowcol = [];
    rowcol.push({
        key: "No. of Rows",
        value: obj.BoltDetails.NoofBoltRows.N
    });
    rowcol.push({
        key: "Row Spacing",
        value: getImeprialFromMM(obj.BoltDetails.BoltSpacingRows.Default.mm)
    });
    B2GBolted.push({ cols: rowcol });

    if (gageRequired) {

        rowcol.push({
            key: "Gage ype",
            value: obj.BoltDetails.BoltLocationOnBeam.DistanceFromCenterOfBeam
        });
        rowcol.push({
            key: "Gage Value",
            value: getImeprialFromMM(obj.BoltDetails.BoltLocationOnBeam.Value.mm)
        });
        B2GBolted.push({ cols: rowcol });

    }

    return B2GBolted;
}

function getG2BHoleType(item) {
    var beamRows = { rows: [] };
    if (item.GussetToBeam.Type == "INDIVIDUAL_CONNECTION") {
        beamRows.rows = beamRows.rows.concat(getG2BHoleDetails(item.GussetToBeam.GussetToBeam1, item.GussetToBeam.GussetToBeam1.Priority[0].P1));
        beamRows.rows = beamRows.rows.concat(getG2BHoleDetails(item.GussetToBeam.GussetToBeam2, item.GussetToBeam.GussetToBeam2.Priority[0].P1));
    }
    else {
        for (x in item.GussetToBeam.GussetToBeam1.Priority) {
            var JsonKey = "P" + (parseInt(x) + 1);
            var type = item.GussetToBeam.GussetToBeam1.Priority[x][JsonKey];
            beamRows.rows = beamRows.rows.concat(getG2BHoleDetails(item.GussetToBeam.GussetToBeam1, type));
        }
    }

    return beamRows;
}

function getG2BHoleDetails(GussetToBeam, ConnectionType) {

    var rows = [];
    switch (ConnectionType) {
        case "SingleClipAngle":
            if (GussetToBeam.SingleClipAngle.Details.SupportSide.includes("BOLTED")) {

                rows.push(getG2BHoleDetailsSupportSupported(GussetToBeam.SingleClipAngle.Details.Support));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.SingleClipAngle.Details.SupportedSide.includes("BOLTED")) {

                rows.push(getG2BHoleDetailsSupportSupported(GussetToBeam.SingleClipAngle.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "DoubleClipAngle":
            if (GussetToBeam.DoubleClipAngle.Details.SupportSide.includes("BOLTED")) {

                rows.push(getG2BHoleDetailsSupportSupported(GussetToBeam.DoubleClipAngle.Details.Support));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.DoubleClipAngle.Details.SupportedSide.includes("BOLTED")) {

                rows.push(getG2BHoleDetailsSupportSupported(GussetToBeam.DoubleClipAngle.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "ShearPlate":
            if (GussetToBeam.ShearPlate.Details.SupportSide.includes("BOLTED")) {

                rows.push(getG2BHoleDetailsSupportSupportedShearPlate(GussetToBeam.ShearPlate.Details.Support));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.ShearPlate.Details.SupportedSide.includes("BOLTED")) {

                rows.push(getG2BHoleDetailsSupportSupportedShearPlate(GussetToBeam.ShearPlate.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "DirectlyWelded":
            rows.push("");
            rows.push("");
            break;
        case "DirectlyBolted":
            rows.push(getG2BHoleDetailsSupportSupportedDirectlyBolted(GussetToBeam.DirectlyBolted));
            rows.push("");
            break;

    }
    return rows;
}

function getG2BHoleDetailsSupportSupported(obj) {
    var values = [];
    values.push({
        key: "Beam",
        value: obj.BoltDetails.HoleType.Beam.dhh
    });
    values.push({
        key: "Clip Angle",
        value: obj.BoltDetails.HoleType.Angle.dhh
    });
    return values;
}
function getG2BHoleDetailsSupportSupportedShearPlate(obj) {
    var values = [];
    values.push({
        key: "Gusset",
        value: obj.BoltDetails.HoleType.Gusset.dhh
    });
    values.push({
        key: "Plate",
        value: obj.BoltDetails.HoleType.ShearPl.dhh
    });
    return values;
}
function getG2BHoleDetailsSupportSupportedDirectlyBolted(obj) {
    var values = [];
    values.push({
        key: "Beam",
        value: obj.Details.BoltDetails.HoleType.Beam.dhh
    });
    values.push({
        key: "Gusset",
        value: obj.Details.BoltDetails.HoleType.Gusset.dhh
    });
    return values;
}

function getG2BConnectionParameter(item) {

    var beamRows = { rows: [] };
    if (item.GussetToBeam.Type == "INDIVIDUAL_CONNECTION") {
        beamRows.rows = beamRows.rows.concat(getG2BConnectionParamDetails(item.GussetToBeam.GussetToBeam1, item.GussetToBeam.GussetToBeam1.Priority[0].P1));
        beamRows.rows = beamRows.rows.concat(getG2BConnectionParamDetails(item.GussetToBeam.GussetToBeam2, item.GussetToBeam.GussetToBeam2.Priority[0].P1));
    }
    else {
        for (x in item.GussetToBeam.GussetToBeam1.Priority) {
            var JsonKey = "P" + (parseInt(x) + 1);
            var type = item.GussetToBeam.GussetToBeam1.Priority[x][JsonKey];
            beamRows.rows = beamRows.rows.concat(getG2BConnectionParamDetails(item.GussetToBeam.GussetToBeam1, type));
        }
    }

    return beamRows;
}

function getG2BConnectionParamDetails(GussetToBeam, ConnectionType) {

    var rows = [];
    switch (ConnectionType) {
        case "SingleClipAngle":

            if (GussetToBeam.SingleClipAngle.Details.SupportSide.includes("BOLTED")) {

                rows.push(getG2BConnectionParamSupportSupported(GussetToBeam.SingleClipAngle.Details.Support));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.SingleClipAngle.Details.SupportedSide.includes("BOLTED")) {

                rows.push(getG2BConnectionParamSupportSupported(GussetToBeam.SingleClipAngle.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "DoubleClipAngle":
            if (GussetToBeam.DoubleClipAngle.Details.SupportSide.includes("BOLTED")) {

                rows.push(getG2BConnectionParamSupportSupported(GussetToBeam.DoubleClipAngle.Details.Support));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.DoubleClipAngle.Details.SupportedSide.includes("BOLTED")) {

                rows.push(getG2BConnectionParamSupportSupported(GussetToBeam.DoubleClipAngle.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "ShearPlate":
            if (GussetToBeam.ShearPlate.Details.SupportSide.includes("BOLTED")) {

                rows.push(getG2BConnectionParamSupportSupportedShearPlate(GussetToBeam.ShearPlate.Details.Support));
            }
            else if (GussetToBeam.ShearPlate.Details.SupportSide.includes("WELDED") && GussetToBeam.ShearPlate.Details.SupportedSide.includes("WELDED")) {

                rows.push(getG2BConnectionParamSupportSupportedShearPlateWelded(GussetToBeam.ShearPlate.Details));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.ShearPlate.Details.SupportedSide.includes("BOLTED")) {

                rows.push(getG2BConnectionParamSupportSupportedShearPlate(GussetToBeam.ShearPlate.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "DirectlyBolted":
            rows.push(getG2BConnectionParamDirectlyBolted(GussetToBeam.DirectlyBolted.Details));
            rows.push("");
            //Added Empty Row for Allignment
            break;
        case "DirectlyWelded":
            rows.push(getG2BConnectionParamDirectlyWelded(GussetToBeam.DirectlyWelded.Details));
            rows.push("");
            //Added Empty Row for Allignment
            break;
        case "Chevron":
            if (GussetToBeam.Chevron.Details.Stiffeners.Required) {
                rows.push(getStiffenerParameters(GussetToBeam.Chevron.Details.Stiffeners.GussetEndStiffeners));
                rows.push("");
            }
            //Added Empty Row for Allignment
            break;
        case "KneeBrace":
            if (GussetToBeam.KneeBrace.KB.Details.Stiffeners.Required) {
                rows.push(getStiffenerParameters(GussetToBeam.KneeBrace.KB.Details.Stiffeners));
                rows.push(getStiffenerWorkPoitDetails(GussetToBeam.KneeBrace.KB.Details.Stiffeners));
            }
            //Added Empty Row for Allignment
            break
    }
    return rows;

}
function getStiffenerWorkPoitDetails(obj) {
    var values = [];
    values.push({
        key: "Type",
        value: obj.Position
    });
    if (obj.Position == "Gusset Ends") {
        values.push({
            key: "Stiffener to Gusset",
            value: obj.WeldDetails.StiffenerToGusset.wtype
        });
        values.push({
            key: "Size",
            value: getImeprialFromMM(obj.WeldDetails.StiffenerToGusset.WeldSize.mm)
        });
        values.push({
            key: "TailInfo",
            value: obj.WeldDetails.StiffenerToGusset.TailInfo
        });
    }
    return values;
}
function getStiffenerParameters(obj) {
    var values = [];
    values.push({
        key: "Stiffener Thickness",
        value: getImeprialFromMM(obj.Thickness.mm)
    });

    values.push({
        key: "Grade",
        value: obj.Grade
    });
    if (obj.Width) {
        values.push({
            key: "Width",
            value: obj.Width.Type
        });
        values.push({
            key: "Value",
            value: getImeprialFromMM(obj.Width.Value.mm)
        });
    }
    return values;
}

function getG2BConnectionParamDirectlyWelded(obj) {
    var values = [];
    values.push({
        key: "Weld Length",
        value: getImeprialFromMM(obj.WeldDetails.WeldLength.mm)
    });

    values.push({
        key: "Overlap Length",
        value: getImeprialFromMM(obj.WeldDetails.OverlapLength.mm)
    });

    return values;

}

function getG2BConnectionParamDirectlyBolted(obj) {
    var values = [];
    values.push({
        key: "Edge Distance Horizontal",
        value: getImeprialFromMM(obj.BoltDetails.EdgeDistance.Horizontal.mm)
    });

    values.push({
        key: "Edge Distance Vertical",
        value: getImeprialFromMM(obj.BoltDetails.EdgeDistance.Vertical.mm)
    });

    return values;

}
function getG2BConnectionParamSupportSupported(obj) {

    var values = [];
    values.push({
        key: "Gage Type",
        value: obj.BoltDetails.GageDetails.BoltGage
    });
    if (obj.BoltDetails.GageDetails.BoltGage != "GOL") {
        values.push({
            key: "Gage Value",
            value: getImeprialFromMM(obj.BoltDetails.GageDetails.Value.mm)
        });
    }
    values.push({
        key: "Edge Distance",
        value: getImeprialFromMM(obj.BoltDetails.EdgeDistance.Angle.mm)
    });
    return values;

}

function getG2BConnectionParamSupportSupportedShearPlate(obj) {

    var values = [];
    values.push({
        key: "Gusset",
        value: getImeprialFromMM(obj.BoltDetails.EdgeDistance.Gusset.mm)
    });
    values.push({
        key: "Plate Horizontal",
        value: getImeprialFromMM(obj.BoltDetails.EdgeDistance.Horizontal.mm)
    });
    values.push({
        key: "Plate Vertical",
        value: getImeprialFromMM(obj.BoltDetails.EdgeDistance.Vertical.mm)
    });
    return values;

}

function getG2BConnectionParamSupportSupportedShearPlateWelded(obj) {
    var values = [];
    values.push({
        key: "Plate Length",
        value: getImeprialFromMM(obj.PlWeldDimensions.Length.mm)
    });
    values.push({
        key: "Plate Width",
        value: getImeprialFromMM(obj.PlWeldDimensions.Width.mm)
    });
    return values;
}

function getG2BWelded(item) {

    var beamRows = { rows: [] };
    if (item.GussetToBeam.Type == "INDIVIDUAL_CONNECTION") {
        if (item.GussetToBeam.GussetToBeam1.Priority[0].P1 == "Chevron") {
            if (item.GussetToBeam.GussetToBeam1.Chevron.Details.Stiffeners.Required) {

                beamRows.rows.push(getG2BWeldedDetailsType3_4Flange(item.GussetToBeam.GussetToBeam1.Chevron.Details.Stiffeners.GussetEndStiffeners));
                beamRows.rows.push(getG2BWeldedDetailsType3_4Web(item.GussetToBeam.GussetToBeam1.Chevron.Details.Stiffeners.GussetEndStiffeners));
            }
        }
        else if (item.GussetToBeam.GussetToBeam1.Priority[0].P1 == "KneeBrace") {
            if (item.GussetToBeam.GussetToBeam1.KneeBrace.KB.Details.Stiffeners.Required) {
                beamRows.rows.push(getG2BWeldedDetailsType3_4Flange(item.GussetToBeam.GussetToBeam1.KneeBrace.KB.Details.Stiffeners));
                beamRows.rows.push(getG2BWeldedDetailsType3_4Web(item.GussetToBeam.GussetToBeam1.KneeBrace.KB.Details.Stiffeners));
            }
        }
        else {
            beamRows.rows = beamRows.rows.concat(getG2BWeldedDetails(item.GussetToBeam.GussetToBeam1, item.GussetToBeam.GussetToBeam1.Priority[0].P1));
            beamRows.rows = beamRows.rows.concat(getG2BWeldedDetails(item.GussetToBeam.GussetToBeam2, item.GussetToBeam.GussetToBeam2.Priority[0].P1));
        }

    }
    else {

        for (x in item.GussetToBeam.GussetToBeam1.Priority) {
            var JsonKey = "P" + (parseInt(x) + 1);
            var type = item.GussetToBeam.GussetToBeam1.Priority[x][JsonKey];
            if (type == "Chevron") {
                if (item.GussetToBeam.GussetToBeam1.Chevron.Details.Stiffeners.Required) {

                    beamRows.rows.push(getG2BWeldedDetailsType3_4Flange(item.GussetToBeam.GussetToBeam1.Chevron.Details.Stiffeners.GussetEndStiffeners));
                    beamRows.rows.push(getG2BWeldedDetailsType3_4Web(item.GussetToBeam.GussetToBeam1.Chevron.Details.Stiffeners.GussetEndStiffeners));
                }
            }
            else if (type == "KneeBrace") {
                if (item.GussetToBeam.GussetToBeam1.KneeBrace.KB.Details.Stiffeners.Required) {
                    beamRows.rows.push(getG2BWeldedDetailsType3_4Flange(item.GussetToBeam.GussetToBeam1.KneeBrace.KB.Details.Stiffeners));
                    beamRows.rows.push(getG2BWeldedDetailsType3_4Web(item.GussetToBeam.GussetToBeam1.KneeBrace.KB.Details.Stiffeners));
                }
            }
            else {
                beamRows.rows = beamRows.rows.concat(getG2BWeldedDetails(item.GussetToBeam.GussetToBeam1, type));
            }

        }

    }
    return beamRows;
}

function getG2BWeldedDetailsType3_4Flange(obj) {

    var flangeRows = [];
    flangeRows.push({
        key: "Stiffener to Flange",
        value: obj.WeldDetails.StiffenerToFlange.wtype
    });
    flangeRows.push({
        key: "Size",
        value: getImeprialFromMM(obj.WeldDetails.StiffenerToFlange.WeldSize.mm)
    });
    flangeRows.push({
        key: "Tail info",
        value: obj.WeldDetails.StiffenerToFlange.TailInfo
    });

    return flangeRows;
}

function getG2BWeldedDetailsType3_4Web(obj) {

    var webRows = [];
    webRows.push({
        key: "Stiffener to Web",
        value: obj.WeldDetails.StiffenerToWeb.wtype
    });
    webRows.push({
        key: "Size",
        value: getImeprialFromMM(obj.WeldDetails.StiffenerToWeb.WeldSize.mm)
    });
    webRows.push({
        key: "Tail info",
        value: obj.WeldDetails.StiffenerToWeb.TailInfo
    });

    return webRows;
}
function getG2BWeldedDetails(GussetToBeam, ConnectionType) {

    var rows = [];
    switch (ConnectionType) {
        case "SingleClipAngle":
            if (GussetToBeam.SingleClipAngle.Details.SupportSide.includes("WELDED")) {

                rows.push(getG2BWeldedSupportSupported(GussetToBeam.SingleClipAngle.Details.Support));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.SingleClipAngle.Details.SupportedSide.includes("WELDED")) {

                rows.push(getG2BWeldedSupportSupported(GussetToBeam.SingleClipAngle.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "DoubleClipAngle":
            if (GussetToBeam.DoubleClipAngle.Details.SupportSide.includes("WELDED")) {

                rows.push(getG2BWeldedSupportSupported(GussetToBeam.DoubleClipAngle.Details.Support));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.DoubleClipAngle.Details.SupportedSide.includes("WELDED")) {

                rows.push(getG2BWeldedSupportSupported(GussetToBeam.DoubleClipAngle.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "ShearPlate":
            if (GussetToBeam.ShearPlate.Details.SupportSide.includes("WELDED")) {

                rows.push(getG2BWeldedSupportShearPlate(GussetToBeam.ShearPlate.Details.Support));
            }
            else {
                rows.push("");
            }
            if (GussetToBeam.ShearPlate.Details.SupportedSide.includes("WELDED")) {

                rows.push(getG2BWeldedSupportSupported(GussetToBeam.ShearPlate.Details.Supported));
            }
            else {
                rows.push("");
            }
            break;
        case "DirectlyBolted":
            // Adding Empty rows for allignment purpose
            rows.push("");
            rows.push("");
            break;
        case "DirectlyWelded":
            // Adding Empty rows for allignment purpose
            rows.push(getG2BWeldedDirectlyWelded(GussetToBeam.DirectlyWelded.Details));
            rows.push("");
    }

    return rows;

}

function getG2BWeldedSupportSupported(obj) {

    var values = [];
    values.push({
        key: "Type",
        value: obj.WeldDetails.wtype
    });
    values.push({
        key: "Size",
        value: getImeprialFromMM(obj.WeldDetails.WeldSize.mm)
    });
    if (obj.WeldDetails.WeldLength && obj.WeldDetails.WeldLength.mm > 0) {
        values.push({
            key: "Length",
            value: getImeprialFromMM(obj.WeldDetails.WeldLength.mm)
        });
    }
    values.push({
        key: "Tail Info",
        value: obj.WeldDetails.TailInfo
    });
    values.push({
        key: "Seal Weld",
        value: obj.WeldDetails.SealWeld
    });
    return values;
}

function getG2BWeldedSupportShearPlate(obj) {

    var values = [];
    values.push({
        key: "Type",
        value: obj.WeldDetails.AlongLength.wtype
    });
    values.push({
        key: "Size",
        value: getImeprialFromMM(obj.WeldDetails.AlongLength.WeldSize.mm)
    });
    values.push({
        key: "Tail Info",
        value: obj.WeldDetails.AlongLength.TailInfo
    });
    return values;

}

function getG2BWeldedDirectlyWelded(obj) {

    var values = [];
    values.push({
        key: "Type",
        value: obj.WeldDetails.wtype
    });
    values.push({
        key: "Size",
        value: getImeprialFromMM(obj.WeldDetails.WeldSize.mm)
    });
    values.push({
        key: "Tail Info",
        value: obj.WeldDetails.TailInfo
    });
    values.push({
        key: "Seal Weld",
        value: obj.WeldDetails.SealWeld
    });
    return values;

}

function getG2BCope(item) {

    var beamRows = { rows: [] };
    if (item.GussetToBeam.Type == "INDIVIDUAL_CONNECTION") {

        beamRows.rows = beamRows.rows.concat(getG2BCopeDetails(item.GussetToBeam.GussetToBeam1, item.GussetToBeam.GussetToBeam1.Priority[0].P1));
        beamRows.rows = beamRows.rows.concat(getG2BCopeDetails(item.GussetToBeam.GussetToBeam2, item.GussetToBeam.GussetToBeam2.Priority[0].P1));
    }
    else {
        for (x in item.GussetToBeam.GussetToBeam1.Priority) {
            var JsonKey = "P" + (parseInt(x) + 1);
            var type = item.GussetToBeam.GussetToBeam1.Priority[x][JsonKey];
            beamRows.rows = beamRows.rows.concat(getG2BCopeDetails(item.GussetToBeam.GussetToBeam1, type));
        }
    }

    return beamRows;
}

function getG2BCopeDetails(GussetToBeam, ConnectionType) {

    switch (ConnectionType) {
        case "SingleClipAngle":
            return getTypeCopeDetails(GussetToBeam.SingleClipAngle);
        case "DoubleClipAngle":
            return getTypeCopeDetails(GussetToBeam.DoubleClipAngle);
        case "ShearPlate":
            return getTypeCopeDetails(GussetToBeam.ShearPlate);
        case "DirectlyBolted":
            return getTypeCopeDetails(GussetToBeam.DirectlyBolted);
        case "DirectlyWelded":
            return getTypeCopeDetails(GussetToBeam.DirectlyWelded);
    }

}

function getTypeCopeDetails(obj) {

    var values = [];
    if (obj.Details.GussetCope.Required) {

        values.push({
            key: "Type",
            value: obj.Details.GussetCope.CopeMethod
        });

        if (obj.Details.GussetCope.CopeMethod == "MAX_VALUE") {

            values.push({
                key: "C1",
                value: getImeprialFromMM(obj.Details.GussetCope.MaxCope.C1.mm)
            });
            values.push({
                key: "C2",
                value: getImeprialFromMM(obj.Details.GussetCope.MaxCope.C2.mm)
            });
        }
        else if (obj.Details.GussetCope.CopeMethod == "RANGE") {


            values.push({
                key: "From C1",
                value: getImeprialFromMM(obje.Details.GussetCope.Range[0].C1.From.mm)
            });
            values.push({
                key: "TO C1",
                value: getImeprialFromMM(obje.Details.GussetCope.Range[0].C1.To.mm)
            });
            values.push({
                key: "L1",
                value: getImeprialFromMM(obje.Details.GussetCope.Range[0].L1.mm)
            });

            values.push({
                key: "From C2",
                value: getImeprialFromMM(obje.Details.GussetCope.Range[0].C2.From.mm)
            });
            values.push({
                key: "TO C2",
                value: getImeprialFromMM(obje.Details.GussetCope.Range[0].C2.To.mm)
            });
            values.push({
                key: "L2",
                value: getImeprialFromMM(obje.Details.GussetCope.Range[0].L2.mm)
            });

        }
    }
    return values;
}