function GetSampleJson() {
    return [{
        "ConnectionMark": "HB1",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "1"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "GOL",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "1",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 304.79999999999995
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB2",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "2"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "2",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 57.15
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "1",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 31.75
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "1",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 304.79999999999995
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB3",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "DOUBLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "2"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "2",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 57.15
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "1",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 31.75
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "5",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 127
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB4",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "DOUBLE",
                "AngleBackToBack": "VERTICAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "4"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "5",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 127
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "1",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 31.75
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "5",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 127
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB5",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "DOUBLE",
                "AngleBackToBack": "VERTICAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "2"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "5",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 127
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "1",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 31.75
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "5",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 127
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB6",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_WELDED",
                "BoltDetails": {},
                "WeldDetails": {
                    "wtype": "FILLET",
                    "WeldSize": {
                        "weld_fra": "1/8",
                        "weld": "0.125",
                        "mm": 3.175
                    },
                    "WeldLength": {
                        "ft": "",
                        "in": "8",
                        "fr_fra": "0",
                        "fr": "0.0",
                        "mm": 203.2
                    },
                    "TailInfo": "",
                    "SealWeld": false
                }
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "DoubleClipAngle"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "1",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 304.79999999999995
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "null",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "DoubleClipAngle"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB7",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_WELDED",
                "BoltDetails": {},
                "WeldDetails": {
                    "wtype": "FILLET",
                    "WeldSize": {
                        "weld_fra": "1/8",
                        "weld": "0.125",
                        "mm": 3.175
                    },
                    "WeldLength": {
                        "ft": "",
                        "in": "8",
                        "fr_fra": "0",
                        "fr": "0.0",
                        "mm": 203.2
                    },
                    "TailInfo": "",
                    "SealWeld": false
                }
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "DoubleClipAngle"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "1",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 304.79999999999995
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "DoubleClipAngle"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB8",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_WELDED",
                "BoltDetails": {},
                "WeldDetails": {
                    "wtype": "FILLET",
                    "WeldSize": {
                        "weld_fra": "1/8",
                        "weld": "0.125",
                        "mm": 3.175
                    },
                    "WeldLength": {
                        "ft": "",
                        "in": "8",
                        "fr_fra": "0",
                        "fr": "0.0",
                        "mm": 203.2
                    },
                    "TailInfo": "",
                    "SealWeld": false
                }
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "DoubleClipAngle"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "null",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "DoubleClipAngle"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB9",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_WELDED",
                "BoltDetails": {},
                "WeldDetails": {
                    "wtype": "FILLET",
                    "WeldSize": {
                        "weld_fra": "1/8",
                        "weld": "0.125",
                        "mm": 3.175
                    },
                    "WeldLength": {
                        "ft": "",
                        "in": "8",
                        "fr_fra": "0",
                        "fr": "0.0",
                        "mm": 203.2
                    },
                    "TailInfo": "",
                    "SealWeld": false
                }
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "DoubleClipAngle"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "ShearPlate"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {
                    "Details": {
                        "Thickness": {
                            "tp_fra": "1/4",
                            "tp": "0.25",
                            "mm": 6.35
                        },
                        "Grade": "A36",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "PlWeldDimensions": {
                            "Length": {
                                "ft": "",
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Width": {
                                "ft": "",
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "Support": {
                            "WeldDetails": {
                                "AlongLength": {
                                    "wtype": "FILLET",
                                    "WeldSize": {
                                        "weld_fra": "1/8",
                                        "weld": "0.125",
                                        "mm": 3.175
                                    },
                                    "TailInfo": "3 SIDES"
                                }
                            },
                            "BoltDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Gusset": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    },
                                    "Horizontal": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    },
                                    "Vertical": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Gusset": {
                                        "dhh": "STD"
                                    },
                                    "ShearPl": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB10",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "WT",
            "Angle": {},
            "WT": {
                "ToeType": "HORIZONTAL",
                "FlangeCutClearance": {
                    "in": "1",
                    "fr_fra": "0",
                    "fr": "0.0",
                    "mm": 25.4
                },
                "Profile": "WT4",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "1"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "2",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 57.15
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "ShearPlate"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {
                    "Details": {
                        "Thickness": {
                            "tp_fra": "1/2",
                            "tp": "0.5",
                            "mm": 12.7
                        },
                        "Grade": "A36",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "SHOP_WELDED",
                        "PlWeldDimensions": {
                            "Length": {
                                "ft": "1",
                                "in": "2",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 355.59999999999997
                            },
                            "Width": {
                                "ft": "",
                                "in": "6",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 152.39999999999998
                            }
                        },
                        "Support": {
                            "WeldDetails": {
                                "AlongLength": {
                                    "wtype": "null",
                                    "WeldSize": {
                                        "weld_fra": "1/8",
                                        "weld": "0.125",
                                        "mm": 3.175
                                    },
                                    "TailInfo": "3 SIDES"
                                }
                            },
                            "BoltDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "null",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                }],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB11",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "WT",
            "Angle": {},
            "WT": {
                "ToeType": "VERTICAL",
                "FlangeCutClearance": {
                    "in": "1",
                    "fr_fra": "0",
                    "fr": "0.0",
                    "mm": 25.4
                },
                "Profile": "WT4",
                "ConnectionMethod": "SHOP_WELDED",
                "BoltDetails": {},
                "WeldDetails": {
                    "wtype": "FILLET",
                    "WeldSize": {
                        "weld_fra": "1/8",
                        "weld": "0.125",
                        "mm": 3.175
                    },
                    "WeldLength": {
                        "ft": "",
                        "in": "8",
                        "fr_fra": "0",
                        "fr": "0.0",
                        "mm": 203.2
                    },
                    "TailInfo": "",
                    "SealWeld": false
                }
            },
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "ShearPlate"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {
                    "Details": {
                        "Thickness": {
                            "tp_fra": "1/2",
                            "tp": "0.5",
                            "mm": 12.7
                        },
                        "Grade": "A36",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "PlWeldDimensions": {
                            "Length": {
                                "ft": "1",
                                "in": "2",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 355.59999999999997
                            },
                            "Width": {
                                "ft": "",
                                "in": "6",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 152.39999999999998
                            }
                        },
                        "Support": {
                            "WeldDetails": {
                                "AlongLength": {
                                    "wtype": "null",
                                    "WeldSize": {
                                        "weld_fra": "1/8",
                                        "weld": "0.125",
                                        "mm": 3.175
                                    },
                                    "TailInfo": "3 SIDES"
                                }
                            },
                            "BoltDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "2",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 63.5
                                    },
                                    "Maximum": {
                                        "in": "3",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 88.89999999999999
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 12.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Gusset": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    },
                                    "Horizontal": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    },
                                    "Vertical": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Gusset": {
                                        "dhh": "STD"
                                    },
                                    "ShearPl": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "ShearPlate"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {
                    "Details": {
                        "Thickness": {
                            "tp_fra": "1/2",
                            "tp": "0.5",
                            "mm": 12.7
                        },
                        "Grade": "A36",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "PlWeldDimensions": {
                            "Length": {
                                "ft": "",
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Width": {
                                "ft": "",
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "Support": {
                            "WeldDetails": {
                                "AlongLength": {
                                    "wtype": "null",
                                    "WeldSize": {
                                        "weld_fra": "1/8",
                                        "weld": "0.125",
                                        "mm": 3.175
                                    },
                                    "TailInfo": "3 SIDES"
                                }
                            },
                            "BoltDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": true,
                                    "Minimum": {
                                        "in": "2",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 63.5
                                    },
                                    "Maximum": {
                                        "in": "3",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 88.89999999999999
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 12.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Gusset": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    },
                                    "Horizontal": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    },
                                    "Vertical": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Gusset": {
                                        "dhh": "STD"
                                    },
                                    "ShearPl": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB12",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "W",
            "Angle": {},
            "WT": {},
            "W": {
                "WebOrientation": "HORIZONTAL",
                "Profile": "W8",
                "ConnectionType": {
                    "Web": {
                        "Required": true,
                        "Details": {
                            "Type": "SPLICE_PLATE",
                            "SupportSide": "FIELD_BOLTED",
                            "SupportedSide": "SHOP_BOLTED"
                        }
                    },
                    "Flange": {
                        "Required": true,
                        "Details": {
                            "Type": "CLAW_ANGLE",
                            "SupportSide": "FIELD_BOLTED",
                            "SupportedSide": "SHOP_BOLTED"
                        }
                    }
                },
                "SetBack": {
                    "os_fra": "1/4",
                    "os": "0.25",
                    "mm": 6.35
                },
                "WebConectionDetails": {
                    "ClawAngle": {},
                    "SplicePlate": {
                        "Thickness": {
                            "tp_fra": "1/4",
                            "tp": "0.25",
                            "mm": 6.35
                        },
                        "Grade": "A36",
                        "Type": "",
                        "BoltDetails": {
                            "BoltName": "A325N",
                            "BoGr": "A325N",
                            "BoltDia": {
                                "d1_fra": "3/4",
                                "d1": "0.75",
                                "mm": 19.049999999999997
                            },
                            "NoofBoltRows": {
                                "N": "2"
                            },
                            "NoofBoltColumns": {
                                "N_C": "2"
                            },
                            "BoltSpacingRows": {
                                "Default": {
                                    "in": "3",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 76.19999999999999
                                },
                                "min_max_s": true,
                                "Minimum": {
                                    "in": "2",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 63.5
                                },
                                "Maximum": {
                                    "in": "3",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 88.89999999999999
                                },
                                "Increment": {
                                    "in": "0",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 12.7
                                }
                            },
                            "BoltSpacingColumns": {
                                "Default": {
                                    "in": "3",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 76.19999999999999
                                }
                            },
                            "EdgeDistance": {
                                "Brace": {
                                    "in": "2",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 50.8
                                },
                                "Gusset": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                },
                                "SplicePlate": {
                                    "Horizontal": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    },
                                    "Vertical": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                }
                            },
                            "HoleType": {
                                "Brace": {
                                    "dhh": "STD"
                                },
                                "Gusset": {
                                    "dhh": "STD"
                                },
                                "SplicePlate": {
                                    "dhh": "STD"
                                }
                            }
                        }
                    }
                },
                "FlangeConectionDetails": {
                    "ClawAngle": {
                        "Profile": "L4X4X1/4",
                        "Grade": "A36",
                        "AngleOSL": "SHORT_LEG",
                        "BoltDetails": {
                            "BoltName": "A325N",
                            "BoGr": "A325N",
                            "BoltDia": {
                                "d1_fra": "3/4",
                                "d1": "0.75",
                                "mm": 19.049999999999997
                            },
                            "NoofBoltRows": {
                                "N": "2"
                            },
                            "BoltSpacingRows": {
                                "Default": {
                                    "in": "3",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 76.19999999999999
                                },
                                "min_max_s": true,
                                "Minimum": {
                                    "in": "2",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 63.5
                                },
                                "Maximum": {
                                    "in": "3",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 88.89999999999999
                                },
                                "Increment": {
                                    "in": "0",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 12.7
                                }
                            },
                            "GageDetails": {
                                "Support": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "Supported": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "2",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 63.5
                                    }
                                }
                            },
                            "EdgeDistance": {
                                "Brace": {
                                    "in": "2",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 50.8
                                },
                                "Gusset": {
                                    "in": "2",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 50.8
                                },
                                "ClawAngle": {
                                    "in": "2",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 50.8
                                }
                            },
                            "HoleType": {
                                "Brace": {
                                    "dhh": "STD"
                                },
                                "Gusset": {
                                    "dhh": "STD"
                                },
                                "ClawAngle": {
                                    "dhh": "STD"
                                }
                            }
                        }
                    }
                }
            },
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "DirectlyBolted"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {
                    "Details": {
                        "BoltDetails": {
                            "ConnectionMethod": "FIELD_BOLTED",
                            "BoGr": "A325N",
                            "BoltName": "A325N",
                            "BoltDia": {
                                "d1_fra": "3/4",
                                "d1": "0.75",
                                "mm": 19.049999999999997
                            },
                            "NoofBoltRows": {
                                "N": "3"
                            },
                            "BoltSpacingRows": {
                                "Default": {
                                    "in": "3",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 76.19999999999999
                                },
                                "min_max_s": false,
                                "Minimum": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "Maximum": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "Increment": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                }
                            },
                            "EdgeDistance": {
                                "Horizontal": {
                                    "in": "1",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 38.099999999999994
                                },
                                "Vertical": {
                                    "in": "1",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 38.099999999999994
                                }
                            },
                            "BoltLocationOnBeam": {
                                "DistanceFromCenterOfBeam": "SPECIFIC",
                                "Value": {
                                    "in": "2",
                                    "fr_fra": "1/4",
                                    "fr": "0.25",
                                    "mm": 57.15
                                }
                            },
                            "HoleType": {
                                "Beam": {
                                    "dhh": "STD"
                                },
                                "Gusset": {
                                    "dhh": "STD"
                                }
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "ShearPlate"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {
                    "Details": {
                        "Thickness": {
                            "tp_fra": "1/2",
                            "tp": "0.5",
                            "mm": 12.7
                        },
                        "Grade": "A36",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "SupportSide": "SHOP_WELDED",
                        "SupportedSide": "FIELD_BOLTED",
                        "PlWeldDimensions": {
                            "Length": {
                                "ft": "",
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Width": {
                                "ft": "",
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "Support": {
                            "WeldDetails": {
                                "AlongLength": {
                                    "wtype": "null",
                                    "WeldSize": {
                                        "weld_fra": "1/8",
                                        "weld": "0.125",
                                        "mm": 3.175
                                    },
                                    "TailInfo": "3 SIDES"
                                }
                            },
                            "BoltDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": true,
                                    "Minimum": {
                                        "in": "2",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 63.5
                                    },
                                    "Maximum": {
                                        "in": "3",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 88.89999999999999
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 12.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Gusset": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    },
                                    "Horizontal": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    },
                                    "Vertical": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Gusset": {
                                        "dhh": "STD"
                                    },
                                    "ShearPl": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB14",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "HSS",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {
                "Profile": "HSS6X4",
                "ConnectionType": "KNIFE_PLATE",
                "DirectlyWelded": {},
                "KnifePlate": {
                    "Thickness": {
                        "tp_fra": "1/4",
                        "tp": "0.25",
                        "mm": 6.35
                    },
                    "Grade": "A36",
                    "SupportSide": "FIELD_BOLTED",
                    "SupportedSide": "SHOP_WELDED",
                    "SetBack": {
                        "os_fra": "1/4",
                        "os": "0.25",
                        "mm": 6.35
                    },
                    "BoltDetails": {
                        "BoGr": "A325N",
                        "BoltName": "A325N",
                        "BoltDia": {
                            "d1_fra": "3/4",
                            "d1": "0.75",
                            "mm": 19.049999999999997
                        },
                        "NoofBoltRows": {
                            "N": "2"
                        },
                        "NoofBoltColumns": {
                            "N_C": "2"
                        },
                        "BoltSpacingRows": {
                            "Default": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            },
                            "min_max_s": false,
                            "Minimum": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Maximum": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Increment": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltSpacingColumns": {
                            "Default": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            }
                        },
                        "EdgeDistance": {
                            "Gusset": {
                                "in": "1",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 25.4
                            },
                            "KnifePlate": {
                                "Horizontal": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                },
                                "Vertical": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                }
                            }
                        },
                        "HoleType": {
                            "Gusset": {
                                "dhh": "STD"
                            },
                            "KnifePlate": {
                                "dhh": "STD"
                            }
                        }
                    },
                    "WeldDetails": {
                        "wtype": "FILLET",
                        "WeldSize": {
                            "weld_fra": "1/8",
                            "weld": "0.125",
                            "mm": 3.175
                        },
                        "WeldLength": {
                            "ft": "",
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "TailInfo": ""
                    }
                }
            },
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "DirectlyWelded"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {
                    "Details": {
                        "WeldDetails": {
                            "ConnectionMethod": "SHOP_WELDED",
                            "wtype": "FILLET",
                            "WeldSize": {
                                "weld_fra": "1/8",
                                "weld": "0.125",
                                "mm": 3.175
                            },
                            "WeldLength": {
                                "ft": "1",
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 381
                            },
                            "OverlapLength": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            },
                            "TailInfo": "3 SIDES",
                            "SealWeld": false
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                }
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": "DirectlyBolted"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {
                    "Details": {
                        "BoltDetails": {
                            "ConnectionMethod": "FIELD_BOLTED",
                            "BoGr": "A325N",
                            "BoltName": "A325N",
                            "BoltDia": {
                                "d1_fra": "3/4",
                                "d1": "0.75",
                                "mm": 19.049999999999997
                            },
                            "NoofBoltRows": {
                                "N": "3"
                            },
                            "BoltSpacingRows": {
                                "Default": {
                                    "in": "3",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 76.19999999999999
                                },
                                "min_max_s": false,
                                "Minimum": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "Maximum": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "Increment": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                }
                            },
                            "EdgeDistance": {
                                "Horizontal": {
                                    "in": "1",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 38.099999999999994
                                },
                                "Vertical": {
                                    "in": "1",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 38.099999999999994
                                }
                            },
                            "BoltLocationOnBeam": {
                                "DistanceFromCenterOfBeam": "SPECIFIC",
                                "Value": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                }
                            },
                            "HoleType": {
                                "Beam": {
                                    "dhh": "STD"
                                },
                                "Gusset": {
                                    "dhh": "STD"
                                }
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB15",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "HSS",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {
                "Profile": "HSS6X4",
                "ConnectionType": "DIRECTLY_WELDED",
                "DirectlyWelded": {
                    "ConnectionMethod": "FIELD_WELDED",
                    "wtype": "FILLET",
                    "WeldSize": {
                        "weld_fra": "1/8",
                        "weld": "0.125",
                        "mm": 3.175
                    },
                    "WeldLength": {
                        "ft": "",
                        "in": "3",
                        "fr_fra": "0",
                        "fr": "0.0",
                        "mm": 76.19999999999999
                    },
                    "TailInfo": "",
                    "ErectionBolt": {
                        "BoGr": "A325N",
                        "BoltName": "A325N",
                        "BoltDia": {
                            "d1_fra": "3/4",
                            "d1": "0.75",
                            "mm": 19.049999999999997
                        }
                    }
                },
                "KnifePlate": {
                    "Thickness": {
                        "tp_fra": "1/4",
                        "tp": "0.25",
                        "mm": 6.35
                    },
                    "Grade": "A36",
                    "SupportSide": "FIELD_BOLTED",
                    "SupportedSide": "SHOP_WELDED",
                    "SetBack": {
                        "os_fra": "1/4",
                        "os": "0.25",
                        "mm": 6.35
                    },
                    "BoltDetails": {
                        "BoGr": "A325N",
                        "BoltName": "A325N",
                        "BoltDia": {
                            "d1_fra": "3/4",
                            "d1": "0.75",
                            "mm": 19.049999999999997
                        },
                        "NoofBoltRows": {
                            "N": "2"
                        },
                        "NoofBoltColumns": {
                            "N_C": "2"
                        },
                        "BoltSpacingRows": {
                            "Default": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            },
                            "min_max_s": false,
                            "Minimum": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Maximum": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Increment": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltSpacingColumns": {
                            "Default": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            }
                        },
                        "EdgeDistance": {
                            "Gusset": {
                                "in": "1",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 25.4
                            },
                            "KnifePlate": {
                                "Horizontal": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                },
                                "Vertical": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                }
                            }
                        },
                        "HoleType": {
                            "Gusset": {
                                "dhh": "STD"
                            },
                            "KnifePlate": {
                                "dhh": "STD"
                            }
                        }
                    },
                    "WeldDetails": {
                        "wtype": "FILLET",
                        "WeldSize": {
                            "weld_fra": "1/8",
                            "weld": "0.125",
                            "mm": 3.175
                        },
                        "WeldLength": {
                            "ft": "",
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "TailInfo": ""
                    }
                }
            },
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "GROUP_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "SingleClipAngle"
                },
                {
                    "P2": "DoubleClipAngle"
                },
                {
                    "P3": "DirectlyBolted"
                },
                {
                    "P4": "DirectlyWelded"
                }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "3 SIDES",
                                "SealWeld": true
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DoubleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X3/16",
                        "Grade": "A36",
                        "ClipAngleOSL": "LONG_LEG",
                        "SupportSide": "FIELD_BOLTED",
                        "SupportedSide": "FIELD_BOLTED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "5",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 139.7
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "GOL",
                                    "Value": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "1/2",
                                        "fr": "0.5",
                                        "mm": 38.099999999999994
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "ShearPlate": {},
                "DirectlyBolted": {
                    "Details": {
                        "BoltDetails": {
                            "ConnectionMethod": "FIELD_BOLTED",
                            "BoGr": "A325N",
                            "BoltName": "A325N",
                            "BoltDia": {
                                "d1_fra": "3/4",
                                "d1": "0.75",
                                "mm": 19.049999999999997
                            },
                            "NoofBoltRows": {
                                "N": "3"
                            },
                            "BoltSpacingRows": {
                                "Default": {
                                    "in": "3",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 76.19999999999999
                                },
                                "min_max_s": false,
                                "Minimum": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "Maximum": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "Increment": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                }
                            },
                            "EdgeDistance": {
                                "Horizontal": {
                                    "in": "1",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 38.099999999999994
                                },
                                "Vertical": {
                                    "in": "1",
                                    "fr_fra": "1/2",
                                    "fr": "0.5",
                                    "mm": 38.099999999999994
                                }
                            },
                            "BoltLocationOnBeam": {
                                "DistanceFromCenterOfBeam": "SPECIFIC",
                                "Value": {
                                    "in": "2",
                                    "fr_fra": "1/4",
                                    "fr": "0.25",
                                    "mm": 57.15
                                }
                            },
                            "HoleType": {
                                "Beam": {
                                    "dhh": "STD"
                                },
                                "Gusset": {
                                    "dhh": "STD"
                                }
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                },
                "DirectlyWelded": {
                    "Details": {
                        "WeldDetails": {
                            "ConnectionMethod": "SHOP_WELDED",
                            "wtype": "FILLET",
                            "WeldSize": {
                                "weld_fra": "1/8",
                                "weld": "0.125",
                                "mm": 3.175
                            },
                            "WeldLength": {
                                "ft": "1",
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 381
                            },
                            "OverlapLength": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            },
                            "TailInfo": "3 SIDES",
                            "SealWeld": false
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [{
                                "C1": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L1": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                },
                                "C2": {
                                    "From": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "To": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    },
                                    "L2": {
                                        "ft": "",
                                        "in": "",
                                        "fr_fra": "",
                                        "fr": "",
                                        "mm": ""
                                    }
                                }
                            }]
                        }
                    }
                }
            },
            "GussetToBeam2": {
                "Priority": [],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB16",
        "ConnectionType": "3",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "HSS",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {
                "Profile": "HSS6X4",
                "ConnectionType": "KNIFE_PLATE",
                "DirectlyWelded": {},
                "KnifePlate": {
                    "Thickness": {
                        "tp_fra": "1/4",
                        "tp": "0.25",
                        "mm": 6.35
                    },
                    "Grade": "A36",
                    "SupportSide": "FIELD_BOLTED",
                    "SupportedSide": "SHOP_WELDED",
                    "SetBack": {
                        "os_fra": "1/4",
                        "os": "0.25",
                        "mm": 6.35
                    },
                    "BoltDetails": {
                        "BoGr": "A325N",
                        "BoltName": "A325N",
                        "BoltDia": {
                            "d1_fra": "3/4",
                            "d1": "0.75",
                            "mm": 19.049999999999997
                        },
                        "NoofBoltRows": {
                            "N": "2"
                        },
                        "NoofBoltColumns": {
                            "N_C": "2"
                        },
                        "BoltSpacingRows": {
                            "Default": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            },
                            "min_max_s": false,
                            "Minimum": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Maximum": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Increment": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltSpacingColumns": {
                            "Default": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            }
                        },
                        "EdgeDistance": {
                            "Gusset": {
                                "in": "1",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 25.4
                            },
                            "KnifePlate": {
                                "Horizontal": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                },
                                "Vertical": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                }
                            }
                        },
                        "HoleType": {
                            "Gusset": {
                                "dhh": "STD"
                            },
                            "KnifePlate": {
                                "dhh": "STD"
                            }
                        }
                    },
                    "WeldDetails": {
                        "wtype": "FILLET",
                        "WeldSize": {
                            "weld_fra": "1/8",
                            "weld": "0.125",
                            "mm": 3.175
                        },
                        "WeldLength": {
                            "ft": "",
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "TailInfo": ""
                    }
                }
            },
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "HSS",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {
                "Profile": "HSS6X4",
                "ConnectionType": "KNIFE_PLATE",
                "DirectlyWelded": {},
                "KnifePlate": {
                    "Thickness": {
                        "tp_fra": "1/4",
                        "tp": "0.25",
                        "mm": 6.35
                    },
                    "Grade": "A36",
                    "SupportSide": "FIELD_BOLTED",
                    "SupportedSide": "SHOP_WELDED",
                    "SetBack": {
                        "os_fra": "1/4",
                        "os": "0.25",
                        "mm": 6.35
                    },
                    "BoltDetails": {
                        "BoGr": "A325N",
                        "BoltName": "A325N",
                        "BoltDia": {
                            "d1_fra": "3/4",
                            "d1": "0.75",
                            "mm": 19.049999999999997
                        },
                        "NoofBoltRows": {
                            "N": "2"
                        },
                        "NoofBoltColumns": {
                            "N_C": "2"
                        },
                        "BoltSpacingRows": {
                            "Default": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            },
                            "min_max_s": false,
                            "Minimum": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Maximum": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            },
                            "Increment": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltSpacingColumns": {
                            "Default": {
                                "in": "3",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 76.19999999999999
                            }
                        },
                        "EdgeDistance": {
                            "Gusset": {
                                "in": "1",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 25.4
                            },
                            "KnifePlate": {
                                "Horizontal": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                },
                                "Vertical": {
                                    "in": "1",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 25.4
                                }
                            }
                        },
                        "HoleType": {
                            "Gusset": {
                                "dhh": "STD"
                            },
                            "KnifePlate": {
                                "dhh": "STD"
                            }
                        }
                    },
                    "WeldDetails": {
                        "wtype": "FILLET",
                        "WeldSize": {
                            "weld_fra": "1/8",
                            "weld": "0.125",
                            "mm": 3.175
                        },
                        "WeldLength": {
                            "ft": "",
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "TailInfo": ""
                    }
                }
            },
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "Chevron"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {
                    "Details": {
                        "WeldDetails": {
                            "ConnectionMethod": "SHOP_WELDED",
                            "wtype": "FILLET",
                            "WeldSize": {
                                "weld_fra": "1/8",
                                "weld": "0.125",
                                "mm": 3.175
                            },
                            "WeldLength": {
                                "ft": "1",
                                "in": "6",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 457.2
                            },
                            "TailInfo": ""
                        },
                        "Stiffeners": {
                            "Required": false,
                            "GussetEndStiffeners": {
                                "Thickness": {
                                    "tp_fra": "1/16",
                                    "tp": "0.0625",
                                    "mm": 1.5875
                                },
                                "Width": {
                                    "Type": "SPECIFIC",
                                    "Value": {
                                        "ft": "",
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "Grade": "A36",
                                "WeldDetails": {
                                    "StiffenerToFlange": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToWeb": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToGusset": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    }
                                }
                            },
                            "IntermidiateStiffeners": {
                                "Required": true,
                                "NumberOfStiffeners": "2",
                                "StiffenerType": "SPLIT"
                            }
                        }
                    }
                },
                "KneeBrace": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": ""
                }],
                "SingleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "DoubleClipAngle": {}
            }
        }
    },
    {
        "ConnectionMark": "HB17",
        "ConnectionType": "3",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "2"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "2",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 57.15
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "1",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 31.75
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "2"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "2",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 57.15
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "1",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 31.75
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "Chevron"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {
                    "Details": {
                        "WeldDetails": {
                            "ConnectionMethod": "SHOP_WELDED",
                            "wtype": "FILLET",
                            "WeldSize": {
                                "weld_fra": "1/8",
                                "weld": "0.125",
                                "mm": 3.175
                            },
                            "WeldLength": {
                                "ft": "1",
                                "in": "6",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 457.2
                            },
                            "TailInfo": ""
                        },
                        "Stiffeners": {
                            "Required": true,
                            "GussetEndStiffeners": {
                                "Thickness": {
                                    "tp_fra": "1/4",
                                    "tp": "0.25",
                                    "mm": 6.35
                                },
                                "Width": {
                                    "Type": "SPECIFIC",
                                    "Value": {
                                        "ft": "",
                                        "in": "6",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 152.39999999999998
                                    }
                                },
                                "Grade": "A36",
                                "WeldDetails": {
                                    "StiffenerToFlange": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToWeb": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToGusset": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    }
                                }
                            },
                            "IntermidiateStiffeners": {
                                "Required": true,
                                "NumberOfStiffeners": "2",
                                "StiffenerType": "SPLIT"
                            }
                        }
                    }
                },
                "KneeBrace": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": ""
                }],
                "SingleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "DoubleClipAngle": {}
            }
        }
    },
    {
        "ConnectionMark": "HB18",
        "ConnectionType": "3",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "WT",
            "Angle": {},
            "WT": {
                "ToeType": "HORIZONTAL",
                "FlangeCutClearance": {
                    "in": "1",
                    "fr_fra": "0",
                    "fr": "0.0",
                    "mm": 25.4
                },
                "Profile": "WT4",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "1"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "2",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 57.15
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "1",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 31.75
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "WT",
            "Angle": {},
            "WT": {
                "ToeType": "HORIZONTAL",
                "FlangeCutClearance": {
                    "in": "1",
                    "fr_fra": "0",
                    "fr": "0.0",
                    "mm": 25.4
                },
                "Profile": "WT4",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "1"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "2",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 57.15
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "1",
                                "fr_fra": "1/4",
                                "fr": "0.25",
                                "mm": 31.75
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        },
                        "Gusset": {
                            "in": "2",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 50.8
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "Chevron"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {
                    "Details": {
                        "WeldDetails": {
                            "ConnectionMethod": "SHOP_WELDED",
                            "wtype": "FILLET",
                            "WeldSize": {
                                "weld_fra": "1/8",
                                "weld": "0.125",
                                "mm": 3.175
                            },
                            "WeldLength": {
                                "ft": "1",
                                "in": "6",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 457.2
                            },
                            "TailInfo": ""
                        },
                        "Stiffeners": {
                            "Required": true,
                            "GussetEndStiffeners": {
                                "Thickness": {
                                    "tp_fra": "1/4",
                                    "tp": "0.25",
                                    "mm": 6.35
                                },
                                "Width": {
                                    "Type": "SPECIFIC",
                                    "Value": {
                                        "ft": "",
                                        "in": "6",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 152.39999999999998
                                    }
                                },
                                "Grade": "A36",
                                "WeldDetails": {
                                    "StiffenerToFlange": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToWeb": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToGusset": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    }
                                }
                            },
                            "IntermidiateStiffeners": {
                                "Required": true,
                                "NumberOfStiffeners": "2",
                                "StiffenerType": "SPLIT"
                            }
                        }
                    }
                },
                "KneeBrace": {}
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": ""
                }],
                "SingleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "DoubleClipAngle": {}
            }
        }
    },
    {
        "ConnectionMark": "HB19",
        "ConnectionType": "4",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "1"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "GOL",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "KneeBrace"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {},
                "KneeBrace": {
                    "KB": {
                        "Details": {
                            "WorkpointSpecific": true,
                            "WorkpointDetails": {
                                "Type": "INSIDE_GUSSET",
                                "InsideGusset": {
                                    "L1": {
                                        "ft": "1",
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 304.79999999999995
                                    },
                                    "L2": {
                                        "ft": "1",
                                        "in": "2",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 355.59999999999997
                                    }
                                },
                                "OutsideGusset": {
                                    "L1": {
                                        "ft": "1",
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 304.79999999999995
                                    },
                                    "L2": {
                                        "ft": "1",
                                        "in": "2",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 355.59999999999997
                                    }
                                }
                            },
                            "WeldDetails": {
                                "ConnectionMethod": "SHOP_WELDED",
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "1",
                                    "in": "6",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 457.2
                                },
                                "TailInfo": ""
                            },
                            "Stiffeners": {
                                "Required": true,
                                "Position": "Gusset Ends",
                                "Thickness": {
                                    "tp_fra": "1/4",
                                    "tp": "0.25",
                                    "mm": 6.35
                                },
                                "Grade": "A36",
                                "WeldDetails": {
                                    "StiffenerToFlange": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToWeb": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToGusset": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": "Seal"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": ""
                }],
                "SingleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "DoubleClipAngle": {}
            }
        }
    },
    {
        "ConnectionMark": "HB20",
        "ConnectionType": "4",
        "Gusset": {
            "Thickness": {
                "tp_fra": "1/4",
                "tp": "0.25",
                "mm": 6.35
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "L",
            "Angle": {
                "Type": "SINGLE",
                "AngleBackToBack": "HORIZONTAL",
                "Profile": "L2X2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "1"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "GOL",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [{
                    "P1": "KneeBrace"
                }],
                "SingleClipAngle": {},
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {},
                "KneeBrace": {
                    "KB": {
                        "Details": {
                            "WorkpointSpecific": true,
                            "WorkpointDetails": {
                                "Type": "INSIDE_GUSSET",
                                "InsideGusset": {
                                    "L1": {
                                        "ft": "1",
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 304.79999999999995
                                    },
                                    "L2": {
                                        "ft": "1",
                                        "in": "2",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 355.59999999999997
                                    }
                                },
                                "OutsideGusset": {
                                    "L1": {
                                        "ft": "1",
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 304.79999999999995
                                    },
                                    "L2": {
                                        "ft": "1",
                                        "in": "2",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 355.59999999999997
                                    }
                                }
                            },
                            "WeldDetails": {
                                "ConnectionMethod": "SHOP_WELDED",
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "1",
                                    "in": "6",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 457.2
                                },
                                "TailInfo": ""
                            },
                            "Stiffeners": {
                                "Required": true,
                                "Position": "At Work Point",
                                "Thickness": {
                                    "tp_fra": "1/4",
                                    "tp": "0.25",
                                    "mm": 6.35
                                },
                                "Grade": "A36",
                                "WeldDetails": {
                                    "StiffenerToFlange": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToWeb": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": ""
                                    },
                                    "StiffenerToGusset": {
                                        "wtype": "FILLET",
                                        "WeldSize": {
                                            "weld_fra": "1/8",
                                            "weld": "0.125",
                                            "mm": 3.175
                                        },
                                        "TailInfo": "Seal"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "GussetToBeam2": {
                "Priority": [{
                    "P1": ""
                }],
                "SingleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "DoubleClipAngle": {}
            }
        }
    },

    {
        "ConnectionMark": "HB30",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "3/8",
                "tp": "0.375",
                "mm": 9.524999999999999
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "WT",
            "Angle": {},
            "WT": {
                "ToeType": "VERTICAL",
                "FlangeCutClearance": {
                    "in": "0",
                    "fr_fra": "0",
                    "fr": "0.0",
                    "mm": 0
                },
                "Profile": "WT2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "2"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "SPECIFIC",
                            "CenterToCenterValue": {
                                "in": "4",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 101.6
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB31",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "3/8",
                "tp": "0.375",
                "mm": 9.524999999999999
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "WT",
            "Angle": {},
            "WT": {
                "ToeType": "VERTICAL",
                "FlangeCutClearance": {
                    "in": "0",
                    "fr_fra": "0",
                    "fr": "0.0",
                    "mm": 0
                },
                "Profile": "WT2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "2"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "GOL",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    }, {
        "ConnectionMark": "HB32",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "3/8",
                "tp": "0.375",
                "mm": 9.524999999999999
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "WT",
            "Angle": {},
            "WT": {
                "ToeType": "HORIZONTAL",
                "FlangeCutClearance": {
                    "in": "0",
                    "fr_fra": "1/4",
                    "fr": "0.25",
                    "mm": 6.35
                },
                "Profile": "WT2",
                "ConnectionMethod": "SHOP_BOLTED",
                "BoltDetails": {
                    "BoGr": "A325N",
                    "BoltName": "A325N",
                    "BoltDia": {
                        "d1_fra": "3/4",
                        "d1": "0.75",
                        "mm": 19.049999999999997
                    },
                    "NoofBoltRows": {
                        "N": "3"
                    },
                    "NoofBoltColumns": {
                        "N_C": "1"
                    },
                    "BoltSpacingRow": {
                        "Default": {
                            "in": "3",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 76.19999999999999
                        },
                        "min_max_s": false,
                        "Minimum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Maximum": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        },
                        "Increment": {
                            "in": "0",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 0
                        }
                    },
                    "BoltSpacingColumn": {
                        "BoltColumn1": {
                            "BoltGage": "GOL",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltColumn2": {
                            "BoltGage": "SPECIFIC",
                            "Value": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        },
                        "BoltCenterToCenter": {
                            "BoltGage": "GOL",
                            "CenterToCenterValue": {
                                "in": "0",
                                "fr_fra": "0",
                                "fr": "0.0",
                                "mm": 0
                            }
                        }
                    },
                    "EdgeDistance": {
                        "Brace": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        },
                        "Gusset": {
                            "in": "1",
                            "fr_fra": "0",
                            "fr": "0.0",
                            "mm": 25.4
                        }
                    },
                    "HoleType": {
                        "Brace": {
                            "dhh": "STD"
                        },
                        "Gusset": {
                            "dhh": "STD"
                        }
                    }
                },
                "WeldDetails": {}
            },
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB33",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "3/8",
                "tp": "0.375",
                "mm": 9.524999999999999
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "W",
            "Angle": {},
            "WT": {},
            "W": {
                "WebOrientation": "VERTICAL",
                "Profile": "W4",
                "ConnectionType": {
                    "Web": {
                        "Required": true,
                        "Details": {
                            "Type": "CLAW_ANGLE",
                            "SupportSide": "FIELD_BOLTED",
                            "SupportedSide": "FIELD_BOLTED"
                        }
                    },
                    "Flange": {
                        "Required": false,
                        "Details": {
                            "Type": "CLAW_ANGLE"
                        }
                    }
                },
                "SetBack": {
                    "os_fra": "1/4",
                    "os": "0.25",
                    "mm": 6.35
                },
                "WebConectionDetails": {
                    "ClawAngle": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "AngleOSL": "SHORT_LEG",
                        "BoltDetails": {
                            "BoltName": "A325N",
                            "BoGr": "A325N",
                            "BoltDia": {
                                "d1_fra": "3/4",
                                "d1": "0.75",
                                "mm": 19.049999999999997
                            },
                            "NoofBoltRows": {
                                "N": "3"
                            },
                            "BoltSpacingRows": {
                                "Default": {
                                    "in": "3",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 76.19999999999999
                                },
                                "min_max_s": false,
                                "Minimum": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "Maximum": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "Increment": {
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                }
                            },
                            "GageDetails": {
                                "Support": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "2",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 50.8
                                    }
                                },
                                "Supported": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "2",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 50.8
                                    }
                                }
                            },
                            "EdgeDistance": {
                                "Brace": {
                                    "in": "2",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 50.8
                                },
                                "Gusset": {
                                    "in": "2",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 50.8
                                },
                                "ClawAngle": {
                                    "in": "2",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 50.8
                                }
                            },
                            "HoleType": {
                                "Brace": {
                                    "dhh": "STD"
                                },
                                "Gusset": {
                                    "dhh": "STD"
                                },
                                "ClawAngle": {
                                    "dhh": "STD"
                                }
                            }
                        }
                    },
                    "SplicePlate": {}
                },
                "FlangeConectionDetails": {
                    "ClawAngle": {}
                }
            },
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    {
        "ConnectionMark": "HB34",
        "ConnectionType": "1_OR_2",
        "Gusset": {
            "Thickness": {
                "tp_fra": "3/8",
                "tp": "0.375",
                "mm": 9.524999999999999
            },
            "Grade": "A36"
        },
        "Brace1toGusset": {
            "BraceShape": "HSS",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {
                "Profile": "HSS1.660",
                "ConnectionType": "DIRECTLY_WELDED",
                "DirectlyWelded": {
                    "ConnectionMethod": "FIELD_WELDED",
                    "wtype": "FILLET",
                    "WeldSize": {
                        "weld_fra": "1/8",
                        "weld": "0.125",
                        "mm": 3.175
                    },
                    "WeldLength": {
                        "ft": "",
                        "in": "3",
                        "fr_fra": "0",
                        "fr": "0.0",
                        "mm": 76.19999999999999
                    },
                    "TailInfo": "",
                    "ErectionBolt": {
                        "BoGr": "A325N",
                        "BoltName": "A325N",
                        "BoltDia": {
                            "d1_fra": "3/4",
                            "d1": "0.75",
                            "mm": 19.049999999999997
                        }
                    }
                },
                "KnifePlate": {}
            },
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "Brace2toGusset": {
            "BraceShape": "",
            "Angle": {},
            "WT": {},
            "W": {},
            "HSS": {},
            "MaximumEccentricity": {
                "Required": false,
                "Value": {
                    "ft": "",
                    "in": "",
                    "fr_fra": "",
                    "fr": "",
                    "mm": ""
                }
            }
        },
        "GussetToBeam": {
            "Type": "INDIVIDUAL_CONNECTION",
            "GussetToBeam1": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {}
            },
            "GussetToBeam2": {
                "Priority": [
                    {
                        "P1": "SingleClipAngle"
                    }
                ],
                "SingleClipAngle": {
                    "Details": {
                        "Profile": "L2X2X1/8",
                        "Grade": "A36",
                        "ClipAngleOSL": "SHORT_LEG",
                        "SupportSide": "SHOP_BOLTED",
                        "SupportedSide": "SHOP_WELDED",
                        "BoltStagger": "NA",
                        "Setback": {
                            "os_fra": "1/4",
                            "os": "0.25",
                            "mm": 6.35
                        },
                        "Support": {
                            "BoltDetails": {
                                "BoGr": "A325N",
                                "BoltName": "A325N",
                                "BoltDia": {
                                    "d1_fra": "3/4",
                                    "d1": "0.75",
                                    "mm": 19.049999999999997
                                },
                                "NoofBoltRows": {
                                    "N": "3"
                                },
                                "BoltSpacingRows": {
                                    "Default": {
                                        "in": "3",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 76.19999999999999
                                    },
                                    "min_max_s": false,
                                    "Minimum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Maximum": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    },
                                    "Increment": {
                                        "in": "0",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 0
                                    }
                                },
                                "GageDetails": {
                                    "BoltGage": "SPECIFIC",
                                    "Value": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "EdgeDistance": {
                                    "Angle": {
                                        "in": "1",
                                        "fr_fra": "0",
                                        "fr": "0.0",
                                        "mm": 25.4
                                    }
                                },
                                "HoleType": {
                                    "Beam": {
                                        "dhh": "STD"
                                    },
                                    "Angle": {
                                        "dhh": "STD"
                                    }
                                }
                            },
                            "WeldDetails": {}
                        },
                        "Supported": {
                            "BoltDetails": {},
                            "WeldDetails": {
                                "wtype": "FILLET",
                                "WeldSize": {
                                    "weld_fra": "1/8",
                                    "weld": "0.125",
                                    "mm": 3.175
                                },
                                "WeldLength": {
                                    "ft": "",
                                    "in": "0",
                                    "fr_fra": "0",
                                    "fr": "0.0",
                                    "mm": 0
                                },
                                "TailInfo": "",
                                "SealWeld": false
                            }
                        },
                        "GussetCope": {
                            "Required": false,
                            "CopeMethod": "",
                            "MaxCope": {
                                "C1": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                },
                                "C2": {
                                    "ft": "",
                                    "in": "",
                                    "fr_fra": "",
                                    "fr": "",
                                    "mm": ""
                                }
                            },
                            "Range": [
                                {
                                    "C1": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L1": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    },
                                    "C2": {
                                        "From": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "To": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        },
                                        "L2": {
                                            "ft": "",
                                            "in": "",
                                            "fr_fra": "",
                                            "fr": "",
                                            "mm": ""
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                "DoubleClipAngle": {},
                "ShearPlate": {},
                "DirectlyBolted": {},
                "DirectlyWelded": {},
                "Chevron": {}
            }
        }
    },
    { "ConnectionMark": "HB36", "ConnectionType": "1_OR_2", "Gusset": { "Thickness": { "tp_fra": "3/8", "tp": "0.375", "mm": 9.524999999999999 }, "Grade": "A36" }, "Brace1toGusset": { "BraceShape": "L", "Angle": { "Type": "DOUBLE", "AngleBackToBack": "HORIZONTAL", "Profile": "L2X2", "ConnectionMethod": "SHOP_BOLTED", "BoltDetails": { "BoGr": "A325N", "BoltName": "A325N", "BoltDia": { "d1_fra": "3/4", "d1": "0.75", "mm": 19.049999999999997 }, "NoofBoltRows": { "N": "3" }, "NoofBoltColumns": { "N_C": "1" }, "BoltSpacingRow": { "Default": { "in": "3", "fr_fra": "0", "fr": "0.0", "mm": 76.19999999999999 }, "min_max_s": true, "Minimum": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 }, "Maximum": { "in": "2", "fr_fra": "0", "fr": "0.0", "mm": 50.8 }, "Increment": { "in": "3", "fr_fra": "0", "fr": "0.0", "mm": 76.19999999999999 } }, "BoltSpacingColumn": { "BoltColumn1": { "BoltGage": "GOL", "Value": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 } }, "BoltColumn2": { "BoltGage": "SPECIFIC", "Value": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "BoltCenterToCenter": { "BoltGage": "GOL", "CenterToCenterValue": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 } } }, "EdgeDistance": { "Brace": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 }, "Gusset": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "HoleType": { "Brace": { "dhh": "STD" }, "Gusset": { "dhh": "STD" } } }, "WeldDetails": {} }, "WT": {}, "W": {}, "HSS": {}, "MaximumEccentricity": { "Required": false, "Value": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } } }, "Brace2toGusset": { "BraceShape": "", "Angle": {}, "WT": {}, "W": {}, "HSS": {}, "MaximumEccentricity": { "Required": false, "Value": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } } }, "GussetToBeam": { "Type": "INDIVIDUAL_CONNECTION", "GussetToBeam1": { "Priority": [{ "P1": "SingleClipAngle" }], "SingleClipAngle": { "Details": { "Profile": "L2X2X1/8", "Grade": "A36", "ClipAngleOSL": "SHORT_LEG", "SupportSide": "SHOP_BOLTED", "SupportedSide": "SHOP_WELDED", "BoltStagger": "NA", "Setback": { "os_fra": "1/4", "os": "0.25", "mm": 6.35 }, "Support": { "BoltDetails": { "BoGr": "A325N", "BoltName": "A325N", "BoltDia": { "d1_fra": "3/4", "d1": "0.75", "mm": 19.049999999999997 }, "NoofBoltRows": { "N": "3" }, "BoltSpacingRows": { "Default": { "in": "3", "fr_fra": "0", "fr": "0.0", "mm": 76.19999999999999 }, "min_max_s": false, "Minimum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Maximum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Increment": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 } }, "GageDetails": { "BoltGage": "SPECIFIC", "Value": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "EdgeDistance": { "Angle": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "HoleType": { "Beam": { "dhh": "STD" }, "Angle": { "dhh": "STD" } } }, "WeldDetails": {} }, "Supported": { "BoltDetails": {}, "WeldDetails": { "wtype": "FILLET", "WeldSize": { "weld_fra": "1/8", "weld": "0.125", "mm": 3.175 }, "WeldLength": { "ft": "", "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "TailInfo": "", "SealWeld": false } }, "GussetCope": { "Required": false, "CopeMethod": "", "MaxCope": { "C1": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "C2": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } }, "Range": [{ "C1": { "From": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "To": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "L1": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } }, "C2": { "From": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "To": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "L2": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } } }] } } }, "DoubleClipAngle": {}, "ShearPlate": {}, "DirectlyBolted": {}, "DirectlyWelded": {} }, "GussetToBeam2": { "Priority": [{ "P1": "SingleClipAngle" }], "SingleClipAngle": { "Details": { "Profile": "L2X2X1/8", "Grade": "A36", "ClipAngleOSL": "SHORT_LEG", "SupportSide": "SHOP_BOLTED", "SupportedSide": "SHOP_WELDED", "BoltStagger": "NA", "Setback": { "os_fra": "1/4", "os": "0.25", "mm": 6.35 }, "Support": { "BoltDetails": { "BoGr": "A325N", "BoltName": "A325N", "BoltDia": { "d1_fra": "3/4", "d1": "0.75", "mm": 19.049999999999997 }, "NoofBoltRows": { "N": "3" }, "BoltSpacingRows": { "Default": { "in": "3", "fr_fra": "0", "fr": "0.0", "mm": 76.19999999999999 }, "min_max_s": false, "Minimum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Maximum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Increment": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 } }, "GageDetails": { "BoltGage": "SPECIFIC", "Value": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "EdgeDistance": { "Angle": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "HoleType": { "Beam": { "dhh": "STD" }, "Angle": { "dhh": "STD" } } }, "WeldDetails": {} }, "Supported": { "BoltDetails": {}, "WeldDetails": { "wtype": "FILLET", "WeldSize": { "weld_fra": "1/8", "weld": "0.125", "mm": 3.175 }, "WeldLength": { "ft": "", "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "TailInfo": "", "SealWeld": false } }, "GussetCope": { "Required": false, "CopeMethod": "", "MaxCope": { "C1": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "C2": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } }, "Range": [{ "C1": { "From": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "To": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "L1": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } }, "C2": { "From": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "To": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "L2": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } } }] } } }, "DoubleClipAngle": {}, "ShearPlate": {}, "DirectlyBolted": {}, "DirectlyWelded": {}, "Chevron": {} } } }
        ,
    { "ConnectionMark": "HB35", "ConnectionType": "1_OR_2", "Gusset": { "Thickness": { "tp_fra": "3/8", "tp": "0.375", "mm": 9.524999999999999 }, "Grade": "A36" }, "Brace1toGusset": { "BraceShape": "L", "Angle": { "Type": "DOUBLE", "AngleBackToBack": "VERTICAL", "Profile": "L2X2", "ConnectionMethod": "SHOP_BOLTED", "BoltDetails": { "BoGr": "A325N", "BoltName": "A325N", "BoltDia": { "d1_fra": "3/4", "d1": "0.75", "mm": 19.049999999999997 }, "NoofBoltRows": { "N": "3" }, "NoofBoltColumns": { "N_C": "2" }, "BoltSpacingRow": { "Default": { "in": "3", "fr_fra": "0", "fr": "0.0", "mm": 76.19999999999999 }, "min_max_s": false, "Minimum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Maximum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Increment": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 } }, "BoltSpacingColumn": { "BoltColumn1": { "BoltGage": "SPECIFIC", "Value": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "BoltColumn2": { "BoltGage": "SPECIFIC", "Value": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "BoltCenterToCenter": { "BoltGage": "GOL", "CenterToCenterValue": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 } } }, "EdgeDistance": { "Brace": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 }, "Gusset": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "HoleType": { "Brace": { "dhh": "STD" }, "Gusset": { "dhh": "STD" } } }, "WeldDetails": {} }, "WT": {}, "W": {}, "HSS": {}, "MaximumEccentricity": { "Required": false, "Value": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } } }, "Brace2toGusset": { "BraceShape": "", "Angle": {}, "WT": {}, "W": {}, "HSS": {}, "MaximumEccentricity": { "Required": false, "Value": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } } }, "GussetToBeam": { "Type": "INDIVIDUAL_CONNECTION", "GussetToBeam1": { "Priority": [{ "P1": "SingleClipAngle" }], "SingleClipAngle": { "Details": { "Profile": "L2X2X1/8", "Grade": "A36", "ClipAngleOSL": "SHORT_LEG", "SupportSide": "SHOP_BOLTED", "SupportedSide": "SHOP_WELDED", "BoltStagger": "NA", "Setback": { "os_fra": "1/4", "os": "0.25", "mm": 6.35 }, "Support": { "BoltDetails": { "BoGr": "A325N", "BoltName": "A325N", "BoltDia": { "d1_fra": "3/4", "d1": "0.75", "mm": 19.049999999999997 }, "NoofBoltRows": { "N": "3" }, "BoltSpacingRows": { "Default": { "in": "3", "fr_fra": "0", "fr": "0.0", "mm": 76.19999999999999 }, "min_max_s": false, "Minimum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Maximum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Increment": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 } }, "GageDetails": { "BoltGage": "SPECIFIC", "Value": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "EdgeDistance": { "Angle": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "HoleType": { "Beam": { "dhh": "STD" }, "Angle": { "dhh": "STD" } } }, "WeldDetails": {} }, "Supported": { "BoltDetails": {}, "WeldDetails": { "wtype": "FILLET", "WeldSize": { "weld_fra": "1/8", "weld": "0.125", "mm": 3.175 }, "WeldLength": { "ft": "", "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "TailInfo": "", "SealWeld": false } }, "GussetCope": { "Required": false, "CopeMethod": "", "MaxCope": { "C1": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "C2": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } }, "Range": [{ "C1": { "From": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "To": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "L1": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } }, "C2": { "From": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "To": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "L2": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } } }] } } }, "DoubleClipAngle": {}, "ShearPlate": {}, "DirectlyBolted": {}, "DirectlyWelded": {} }, "GussetToBeam2": { "Priority": [{ "P1": "SingleClipAngle" }], "SingleClipAngle": { "Details": { "Profile": "L2X2X1/8", "Grade": "A36", "ClipAngleOSL": "SHORT_LEG", "SupportSide": "SHOP_BOLTED", "SupportedSide": "SHOP_WELDED", "BoltStagger": "NA", "Setback": { "os_fra": "1/4", "os": "0.25", "mm": 6.35 }, "Support": { "BoltDetails": { "BoGr": "A325N", "BoltName": "A325N", "BoltDia": { "d1_fra": "3/4", "d1": "0.75", "mm": 19.049999999999997 }, "NoofBoltRows": { "N": "3" }, "BoltSpacingRows": { "Default": { "in": "3", "fr_fra": "0", "fr": "0.0", "mm": 76.19999999999999 }, "min_max_s": false, "Minimum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Maximum": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "Increment": { "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 } }, "GageDetails": { "BoltGage": "SPECIFIC", "Value": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "EdgeDistance": { "Angle": { "in": "1", "fr_fra": "0", "fr": "0.0", "mm": 25.4 } }, "HoleType": { "Beam": { "dhh": "STD" }, "Angle": { "dhh": "STD" } } }, "WeldDetails": {} }, "Supported": { "BoltDetails": {}, "WeldDetails": { "wtype": "FILLET", "WeldSize": { "weld_fra": "1/8", "weld": "0.125", "mm": 3.175 }, "WeldLength": { "ft": "", "in": "0", "fr_fra": "0", "fr": "0.0", "mm": 0 }, "TailInfo": "", "SealWeld": false } }, "GussetCope": { "Required": false, "CopeMethod": "", "MaxCope": { "C1": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "C2": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } }, "Range": [{ "C1": { "From": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "To": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "L1": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } }, "C2": { "From": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "To": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" }, "L2": { "ft": "", "in": "", "fr_fra": "", "fr": "", "mm": "" } } }] } } }, "DoubleClipAngle": {}, "ShearPlate": {}, "DirectlyBolted": {}, "DirectlyWelded": {}, "Chevron": {} } } }
    ]

}